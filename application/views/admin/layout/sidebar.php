<section>
	<!-- Left Sidebar -->
	<aside id="leftsidebar" class="sidebar">
		<!-- User Info -->
		<div class="user-info">
			<div class="image">
				<div id="ajax-loading">
					<div class="loader">
						<div class="preloader pl-size-sm">
							<div class="spinner-layer pl-red">
								<div class="circle-clipper left">
									<div class="circle"></div>
								</div>
								<div class="circle-clipper right">
									<div class="circle"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<img src="<?php echo base_url() ?>assets/images/ziz.png" width="48" height="48" id="side_img" />
			</div>
			<div class="info-container">
				<div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $_SESSION['invtr_in']['name'] ?>
				</div>
				<div class="email">
					<?php echo $_SESSION['invtr_in']['email'] ?>
				</div>
				<div class="btn-group user-helper-dropdown">
					<i class="material-icons" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="true">keyboard_arrow_down</i>
					<ul class="dropdown-menu pull-right">
						<li>
							<a href="javascript:void(0);"
								onclick="main_menu('#','master_controller/form/user/<?php echo $_SESSION['invtr_in']['id'] ?>')">
								<i class="material-icons">person</i>Profile</a>
						</li>
						<li>
							<a onclick="logout()" href="javascript:void(0);">
								<i class="material-icons">input</i>Sign Out</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- #User Info -->
		<!-- Menu -->
		<div class="menu">
			<ul class="list">
				<li class="header">NAVIGASI UTAMA</li>

				<?php if(role(['admin'], false)){ ?>
				<li id="home">
					<a onclick="main_menu('#home','main_controller/home')">
						<i class="material-icons">home</i>
						<span>Home</span>
					</a>
				</li>

				<li id="master_data">
					<a class="menu-toggle" id="master_data_toggle">
						<i class="material-icons">list</i>
						<span>Data Master</span>
					</a>
					<ul class="ml-menu">
						<li id="user">
							<a onclick="sub_menu('#master_data','#user','master_controller/user')">Pengguna</a>
						</li>
						<li id="customer">
							<a onclick="sub_menu('#master_data','#customer','master_controller/customer')">Customer</a>
						</li>
						<li id="suplier">
							<a onclick="sub_menu('#master_data','#suplier','master_controller/suplier')">Suplier</a>
						</li>
						<li id="barang">
							<a onclick="sub_menu('#master_data','#barang','master_controller/barang')">Barang</a>
						</li>
					</ul>
				</li>

				<li id="transaksi">
					<a class="menu-toggle" id="transaksi_toggle">
						<i class="material-icons">swap_horiz</i>
						<span>Transaksi</span>
					</a>
					<ul class="ml-menu">
						<li id="barang_masuk">
							<a onclick="sub_menu('#transaksi','#barang_masuk','transaksi_controller/barang_masuk/null')">Barang Masuk</a>
						</li>
						<li id="barang_keluar">
							<a onclick="sub_menu('#transaksi','#barang_keluar','transaksi_controller/barang_keluar/null')">Barang Keluar</a>
						</li>
						<li id="request_user">
							<a onclick="sub_menu('#transaksi','#request_user','request_controller/daftar_request')">Request User</a>
						</li>
					</ul>
				</li>

				<li id="laporan">
					<a class="menu-toggle" id="laporan_toggle">
						<i class="material-icons">book</i>
						<span>Laporan</span>
					</a>
					<ul class="ml-menu">
						<li id="lap_brg_masuk">
							<a onclick="sub_menu('#laporan','#lap_brg_masuk','laporan_controller/barang_masuk/<?php echo $date_now[1].'/'.$date_now[0] ?>')">Barang Masuk</a>
						</li>
						<li id="lap_brg_keluar">
							<a onclick="sub_menu('#laporan','#lap_brg_keluar','laporan_controller/barang_keluar/<?php echo $date_now[1].'/'.$date_now[0] ?>')">Barang Keluar</a>
						</li>
						<li id="lap_request">
							<a onclick="sub_menu('#laporan','#lap_request','laporan_controller/request/<?php echo $date_now[1].'/'.$date_now[0] ?>')">Request</a>
						</li>
					</ul>
				</li>
				<?php }else{?>
				<li id="request">
					<a onclick="main_menu('#request','request_controller/request/null')">
						<i class="material-icons">move_to_inbox</i>
						<span>Permintaan Barang</span>
					</a>
				</li>
				<?php } ?>

			</ul>
		</div>
		<!-- #Menu -->
		<!-- Footer -->
		<div class="legal">
			<div class="copyright">
				&copy; 2019
				<a href="javascript:void(0);">Siperang Hama</a>
			</div>
			<div class="version">
				<b>Version: </b> 1.0.0
			</div>
		</div>
		<!-- #Footer -->
	</aside>
	<!-- #END# Left Sidebar -->
	<!-- Right Sidebar -->
	<aside id="rightsidebar" class="right-sidebar">
		<ul class="nav nav-tabs tab-nav-right" role="tablist">
			<li role="presentation" class="active">
				<a href="#skins" data-toggle="tab">TEMA</a>
			</li>
			<li role="presentation">
				<a href="#settings" data-toggle="tab">PENGATURAN</a>
			</li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active in active" id="skins">
				<ul class="demo-choose-skin">
					<?php if ($theme == 'theme-red') {?>
					<li onclick="setTheme('theme-red')" class="active" data-theme="red">
						<?php } else {?>
					<li onclick="setTheme('theme-red')" data-theme="red">
						<?php }?>
						<div class="red"></div>
						<span>Red</span>
					</li>

					<?php if ($theme == 'theme-pink') {?>
					<li onclick="setTheme('theme-pink')" class="active" data-theme="pink">
						<?php } else {?>
					<li onclick="setTheme('theme-pink')" data-theme="pink">
						<?php }?>
						<div class="pink"></div>
						<span>Pink</span>
					</li>

					<?php if ($theme == 'theme-purple') {?>
					<li onclick="setTheme('theme-purple')" class="active" data-theme="purple">
						<?php } else {?>
					<li onclick="setTheme('theme-purple')" data-theme="purple">
						<?php }?>
						<div class="purple"></div>
						<span>Purple</span>
					</li>

					<?php if ($theme == 'theme-deep-purple') {?>
					<li onclick="setTheme('theme-deep-purple')" class="active" data-theme="deep-purple">
						<?php } else {?>
					<li onclick="setTheme('theme-deep-purple')" data-theme="deep-purple">
						<?php }?>
						<div class="deep-purple"></div>
						<span>Deep Purple</span>
					</li>

					<?php if ($theme == 'theme-indigo') {?>
					<li onclick="setTheme('theme-indigo')" class="active" data-theme="indigo">
						<?php } else {?>
					<li onclick="setTheme('theme-indigo')" data-theme="indigo">
						<?php }?>
						<div class="indigo"></div>
						<span>Indigo</span>
					</li>

					<?php if ($theme == 'theme-blue') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="blue">
						<?php } else {?>
					<li onclick="setTheme('theme-blue')" data-theme="blue">
						<?php }?>
						<div class="blue"></div>
						<span>Blue</span>
					</li>

					<?php if ($theme == 'theme-light-blue') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="light-blue">
						<?php } else {?>
					<li onclick="setTheme('theme-light-blue')" data-theme="light-blue">
						<?php }?>
						<div class="light-blue"></div>
						<span>Light Blue</span>
					</li>

					<?php if ($theme == 'theme-cyan') {?>
					<li onclick="setTheme('theme-blue')" class="active" data-theme="cyan">
						<?php } else {?>
					<li onclick="setTheme('theme-cyan')" data-theme="cyan">
						<?php }?>
						<div class="cyan"></div>
						<span>Cyan</span>
					</li>

					<?php if ($theme == 'theme-teal') {?>
					<li onclick="setTheme('theme-teal')" class="active" data-theme="teal">
						<?php } else {?>
					<li onclick="setTheme('theme-teal')" data-theme="teal">
						<?php }?>
						<div class="teal"></div>
						<span>Teal</span>
					</li>

					<?php if ($theme == 'theme-green') {?>
					<li onclick="setTheme('theme-green')" class="active" data-theme="green">
						<?php } else {?>
					<li onclick="setTheme('theme-green')" data-theme="green">
						<?php }?>
						<div class="green"></div>
						<span>Green</span>
					</li>

					<?php if ($theme == 'theme-light-green') {?>
					<li onclick="setTheme('theme-light-green')" class="active" data-theme="light-green">
						<?php } else {?>
					<li onclick="setTheme('theme-light-green')" data-theme="light-green">
						<?php }?>
						<div class="light-green"></div>
						<span>Light Green</span>
					</li>

					<?php if ($theme == 'theme-lime') {?>
					<li onclick="setTheme('theme-lime')" class="active" data-theme="lime">
						<?php } else {?>
					<li onclick="setTheme('theme-lime')" data-theme="lime">
						<?php }?>
						<div class="lime"></div>
						<span>Lime</span>
					</li>

					<?php if ($theme == 'theme-yellow') {?>
					<li onclick="setTheme('theme-yellow')" class="active" data-theme="yellow">
						<?php } else {?>
					<li onclick="setTheme('theme-yellow')" data-theme="yellow">
						<?php }?>
						<div class="yellow"></div>
						<span>Yellow</span>
					</li>

					<?php if ($theme == 'theme-amber') {?>
					<li onclick="setTheme('theme-amber')" class="active" data-theme="amber">
						<?php } else {?>
					<li onclick="setTheme('theme-amber')" data-theme="amber">
						<?php }?>
						<div class="amber"></div>
						<span>Amber</span>
					</li>

					<?php if ($theme == 'theme-orange') {?>
					<li onclick="setTheme('theme-orange')" class="active" data-theme="orange">
						<?php } else {?>
					<li onclick="setTheme('theme-orange')" data-theme="orange">
						<?php }?>
						<div class="orange"></div>
						<span>Orange</span>
					</li>

					<?php if ($theme == 'theme-deep-orange') {?>
					<li onclick="setTheme('theme-deep-orange')" class="active" data-theme="deep-orange">
						<?php } else {?>
					<li onclick="setTheme('theme-deep-orange')" data-theme="deep-orange">
						<?php }?>
						<div class="deep-orange"></div>
						<span>Deep Orange</span>
					</li>

					<?php if ($theme == 'theme-brown') {?>
					<li onclick="setTheme('theme-brown')" class="active" data-theme="brown">
						<?php } else {?>
					<li onclick="setTheme('theme-brown')" data-theme="brown">
						<?php }?>
						<div class="brown"></div>
						<span>Brown</span>
					</li>

					<?php if ($theme == 'theme-grey') {?>
					<li onclick="setTheme('theme-grey')" class="active" data-theme="grey">
						<?php } else {?>
					<li onclick="setTheme('theme-grey')" data-theme="grey">
						<?php }?>
						<div class="grey"></div>
						<span>Grey</span>
					</li>

					<?php if ($theme == 'theme-blue-grey') {?>
					<li onclick="setTheme('theme-blue-grey')" class="active" data-theme="blue-grey">
						<?php } else {?>
					<li onclick="setTheme('theme-blue-grey')" data-theme="blue-grey">
						<?php }?>
						<div class="blue-grey"></div>
						<span>Blue Grey</span>
					</li>

					<?php if ($theme == 'theme-black') {?>
					<li onclick="setTheme('theme-black')" class="active" data-theme="black">
						<?php } else {?>
					<li onclick="setTheme('theme-black')" data-theme="black">
						<?php }?>
						<div class="black"></div>
						<span>Black</span>
					</li>
				</ul>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="settings">
				<div class="demo-settings">
					<p>Pengaturan Sistem</p>
					<ul class="setting-list">
					
						<li>
							<span>Sistem Email : </span>
							<div class="row">

								<div class="col-md-12">
									<input onkeyup="check_sys_mail()" id="send_mail" type="email" class="form-control"
										value="<?php echo $setting[0]['send_mail'] ?>">
								</div>

							</div>
							<br>
							<span>Password : </span>
							<div class="row">

								<div class="col-md-12">
									<input onkeyup="check_sys_mail()" id="send_pass" type="password"
										class="form-control" value="<?php echo $setting[0]['send_pass'] ?>">
								</div>

							</div>
							<br>
							<button id="sys_btn" onclick="update_sys_mail()" style="border-radius:100px;width:100%;"
								class="btn btn-primary waves-effect">Simpan</button>

						</li>
						
					</ul>
				</div>
			</div>
		</div>
	</aside>
	<!-- #END# Right Sidebar -->
</section>

<script>

	function check_sys_mail() {
		var email = $("#send_mail").val();
		var password = $("#send_pass").val();

		if (email != '' && password != '') {
			$('#sys_btn').removeAttr('disabled');
		} else {
			$('#sys_btn').attr('disabled', 'disabled');
		}
	}

	function update_sys_mail() {
		check_sys_mail();
		var data = {
			email: $("#send_mail").val(),
			password: $("#send_pass").val()
		}
		postData('main_controller/update_sys_mail', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					swal("Success", "Berhasil menyimpan email sistem.", "success");
				} else {
					swal("Oops..!", "Simpan email gagal.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}
</script>