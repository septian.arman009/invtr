<div class="container-fluid">
	<div class="block-header">
		<h2>PENGGUNA</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/user','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						<?php
							if($id == 'null'){
								echo 'Tambah Pengguna';
							}else{
								echo 'Edit Pengguna';
							}
						?>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Nama</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="text" class="form-control"
										id="name" />
								</div>
							</div>
							<div class="form-group">
								<label>Email</label>
								<div class="form-line">
									<input onkeyup="email_check($(this).val())" type="email" class="form-control"
										id="email" placeholder="example (@gmail.com)" />
								</div>
								<div style="display:none;color:red" id="invalid_email">Masukan format email yang benar.
								</div>
								<div style="display:none;color:red" id="used">Email sudah digunakan.</div>
							</div>

							<?php if($id == 'null'){ ?>
							<div class="form-group">
								<label>Password</label>
								<div class="form-line">
									<input onkeyup="check_password()" type="password" class="form-control"
										id="password" />
								</div>
								<div style="display:none;color:red" id="passless">Min 8 karakter.</div>
							</div>

							<div class="form-group">
								<label>Password</label>
								<div class="form-line">
									<input onkeyup="check_password()" type="password" class="form-control"
										id="confirm" />
								</div>
								<div style="display:none;color:red" id="conless">Min 8 karakter.</div>
								<div style="display:none;color:red" id="unmatch">Password tidak sama.</div>
							</div>
							<?php } ?>

							<div class="form-group">
								<label>Otoritas</label>
								<div class="form-line">
									<select class="form-control" id="role_id">
										<?php foreach ($roles as $key => $value) { ?>
										<?php if(role(['admin'], false)){ ?>
										<option value="<?php echo $value['role_id'] ?>">
											<?php echo $value['display_name'] ?></option>
										<?php }else{?>
										<?php if($value['role_id'] != 1 || $value['role_id'] != 2) {?>
										<option value="<?php echo $value['role_id'] ?>">
											<?php echo $value['display_name'] ?></option>
										<?php }?>
										<?php }?>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Simpan</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="userformjs">
	var id = '<?php echo $id ?>';
	var role = '<?php echo $role ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'user_id',
				table: 'users'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#name").val(response.data[0].name);
						$("#email").val(response.data[0].email);
						$("#role_id").val(response.data[0].role_id);
						this.v_email = 'true';
						this.passCheck = 'true';
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		if ($("#name").val() != '' && this.v_email == 'true' && this.passCheck == 'true') {
			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function email_check(email) {
		if (email != '') {
			if (ValidateEmail(email, "#save", "#invalid_email")) {
				var data = {
					id: id,
					value: $("#email").val(),
					column: 'email',
					table: 'users'
				}

				postData('main_controller/single_data_check', data, function (err, response) {
					if (response) {
						var status = response.status;
						if (status == 'success') {
							$("#used").hide();
							this.v_email = 'true';
							setTimeout(() => {
								form_check();
							}, 100);
						} else {
							$("#used").show();
							this.v_email = 'false';
							setTimeout(() => {
								form_check();
							}, 100);
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		} else {
			$("#invalid_email").hide();
			$("#used").hide();
		}
	}

	function check_password() {
		var password = $("#password").val();
		var confirm = $("#confirm").val();

		var passlen = password.length;
		var conlen = confirm.length;

		if (passlen > 0 && passlen < 8) {
			$("#passless").show();
		} else {
			$("#passless").hide();
		}

		if (conlen > 0 && conlen < 8) {
			$("#conless").show();
			$("#unmatch").hide();
		} else {
			$("#conless").hide();
			$("#unmatch").hide();
			if (password.length >= 8 && confirm.length >= 8) {
				if (password == confirm) {
					this.passCheck = 'true';
					$("#unmatch").hide();
					$("#save").removeAttr('disabled');
					setTimeout(() => {
						form_check();
					}, 100);
				} else {
					this.passCheck = 'false';
					$("#unmatch").show();
					setTimeout(() => {
						form_check();
					}, 100);
				}
			} else {
				this.passCheck = 'false';
				setTimeout(() => {
					form_check();
				}, 100);
			}
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'users',
			name: $('#name').val(),
			email: $('#email').val(),
			password: $('#password').val(),
			role_id: $('#role_id').val()
		}

		postData('master_controller/user_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#name").val('');
					$("#email").val('');
					$("#password").val('');
					$("#confirm").val('');
					swal("Success", "Berhasil simpan data pengguna.", "success");
					if (id != 'null') {
						loadView('master_controller/user', '.content');
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Simpan data gagal.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('userformjs').innerHTML = "";
</script>