<div class="container-fluid">
	<div class="block-header">
		<h2>SUPLIER</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/suplier','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						<?php 
                            if($id == 'null') {
                                echo 'Tambah Suplier';
                            }else{
                                echo 'Edit Suplier';
                            }
                        ?>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">

							<div class="form-group">
								<label>Kode Suplier</label>
								<div class="form-line">
									<input readonly type="text" value="<?php echo $kd_suplier ?>" class="form-control"
										id="kd_suplier" />
								</div>
							</div>
							<div class="form-group">
								<label>Nama</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="text" class="form-control"
										id="nama" />
								</div>
							</div>

                            <div class="form-group">
								<label>Alamat</label>
								<div class="form-line">
									<textarea onkeyup="form_check($(this).val())" class="form-control"
										id="alamat"></textarea> 
								</div>
							</div>

                            <div class="form-group">
								<label>No. Telpon</label>
								<div class="form-line">
									<input type="number" onkeyup="form_check($(this).val())" class="form-control"
										id="tlp"> 
								</div>
							</div>

						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Simpan</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="suplierformjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id_suplier',
				table: 'suplier'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#kd_suplier").val(response.data[0].kd_suplier);
						$("#nama").val(response.data[0].nama);
						$("#alamat").val(response.data[0].alamat);
                        $("#tlp").val(response.data[0].tlp);
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		if ($("#nama").val() != '' && $("#alamat").val() != '' 
            && $("#tlp").val() != '') {

			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'suplier',
			kd_suplier: $('#kd_suplier').val(),
			nama: $('#nama').val(),
			alamat: $('#alamat').val(),
			tlp: $('#tlp').val()
		}

		postData('master_controller/suplier_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#nama").val('');
					$("#alamat").val('');
					$("#tlp").val('');
					swal("Success", "Berhasil simpan data suplier.", "success");
					if (id != 'null') {
						loadView('master_controller/suplier', '.content');
					}else{
						loadView("master_controller/form/suplier/null/null", ".content");
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Simpan data gagal.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('suplierformjs').innerHTML = "";
</script>