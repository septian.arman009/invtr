<div class="container-fluid">
	<div class="block-header">
		<h2>BARANG</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
                <div class="body table-responsive">
				<a onclick="loadView('master_controller/form/barang/null/null','.content')" class="btn btn-primary waves-effect"> Tambah Barang </a>
				<a href='laporan_controller/print/barang/null' target="_blank" class="btn btn-primary waves-effect"> Cetak Barang </a>
				<br>
				<br>
				<table id="barang-table" class="table stripe hover">
					<thead>
						<tr>
							<th id="th" width="10%">No</th>
							<th id="th">Kode Barang</th>
							<th id="th">Suplier</th>
							<th id="th">Nama</th>
							<th id="th">Harga</th>
                            <th id="th">Stock</th>
							<th id="th" class="no-sort" width="10%">Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th class="footer">no</th>
                            <th class="footer">Kode Barang</th>
							<th class="footer">Suplier</th>
							<th class="footer">Nama</th>
                            <th class="footer">Harga</th>
                            <th class="footer">Stock</th>
						</tr>
					</tfoot>
				</table>
                </div>
			</div>
		</div>
	</div>

	<script id="barangjs">
		$(document).ready(function () {
			$('#barang-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title + '" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#barang-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'master_controller/barang_table',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});

		function destroy(id) {
			swal({
					title: "Apa kamu yakin ?",
					text: "Data akan dihapus secara permanen.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Hapus Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/barang/id_barang/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/barang', '.content');
    							swal("Success", "Berhasil menghapus data barang.", "success");
							}else{
								swal("Oops", "Hapus gagal.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

        function edit(id)
        {
            loadView('master_controller/form/barang/'+id+'/null','.content');
		}
		
		document.getElementById('barangjs').innerHTML = "";
		
	</script>

	<style>
		#barang-table_filter{
			display:none;
		}
	</style>
