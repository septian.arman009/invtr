<div class="container-fluid">
	<div class="block-header">
		<h2>BARANG</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/barang','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						<?php 
                            if($id == 'null') {
                                echo 'Tambah Barang';
                            }else{
                                echo 'Edit Barang';
                            }
                        ?>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">

							<div class="form-group">
								<label>Kode Barang</label>
								<div class="form-line">
									<input readonly type="text" value="<?php echo $kd_barang ?>" class="form-control"
										id="kd_barang" />
								</div>
							</div>

							<div class="form-group">
								<label>Suplier</label>
								<div class="form-line">
									<select onchange="form_check()" class="form-control" id="kd_suplier">
										<option value="">- Pilih Suplier -</option>
										<?php foreach ($suplier as $key => $value) { ?>
											<option value="<?php echo $value['kd_suplier'] ?>"><?php echo $value['nama'] ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label>Nama</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="text" class="form-control"
										id="nama" />
								</div>
							</div>

							<div class="form-group">
								<label>Satuan</label>
								<div class="form-line">
									<select onchange="form_check($(this).val())" class="form-control"
										id="satuan">
                                        <option value="">- Pilih Satuan -</option>
                                        <option value="ml">ML</option>
                                        <option value="pcs">Pcs</option>
                                    </select>
								</div>
							</div>

							<div class="form-group">
								<label>Harga</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="text" class="form-control"
										id="harga" />
								</div>
							</div>

							<div class="form-group">
								<label>Jumlah Barang</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="number" class="form-control"
										id="jml_barang" />
								</div>
							</div>

						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Simpan</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="barangformjs">
	$(document).ready(function () {
		$('#harga').maskMoney({
			prefix: 'Rp. ',
			thousands: '.',
			decimal: ',',
			precision: 0
		});
	});

	var id = '<?php echo $id ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id_barang',
				table: 'barang'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#kd_barang").val(response.data[0].kd_barang);
						$("#kd_suplier").val(response.data[0].kd_suplier);
						$("#nama").val(response.data[0].nama);
						$("#satuan").val(response.data[0].satuan);
						$("#harga").val(to_rp(response.data[0].harga));
						$("#jml_barang").val(response.data[0].jml_barang);
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		if ($("#kd_barang").val() != '' && $("#kd_suplier").val() != '' && $("#nama").val() != '' &&
			$("#satuan").val() != '' &&$("#harga").val() != '' && $("#jml_barang").val() != '') {

			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'barang',
			kd_barang: $('#kd_barang').val(),
			kd_suplier: $('#kd_suplier').val(),
			nama: $('#nama').val(),
			satuan: $('#satuan').val(),
			harga: $('#harga').val(),
            jml_barang: $('#jml_barang').val()
		}

		postData('master_controller/barang_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					swal("Success", "Berhasil simpan data barang.", "success");
					if (id != 'null') {
						loadView('master_controller/barang', '.content');
					}else{
                        loadView("master_controller/form/barang/null/null", ".content");
                    }
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Simpan data gagal.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('barangformjs').innerHTML = "";
</script>