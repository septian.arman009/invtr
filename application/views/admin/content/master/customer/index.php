<div class="container-fluid">
	<div class="block-header">
		<h2>CUSTOMER</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
                <div class="body table-responsive">
				<a onclick="loadView('master_controller/form/customer/null/null','.content')" class="btn btn-primary waves-effect"> Tambah Customer </a>
				<br>
				<br>
				<table id="customer-table" class="table stripe hover">
					<thead>
						<tr>
							<th id="th" width="10%">No</th>
							<th id="th">Kode Customer</th>
							<th id="th">Nama</th>
							<th id="th">Alamat</th>
							<th id="th">Telpon</th>
							<th id="th">PIC</th>
							<th id="th" class="no-sort" width="10%">Action</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th class="footer">no</th>
                            <th class="footer">Kode Customer</th>
							<th class="footer">Nama</th>
							<th class="footer">Alamat</th>
                            <th class="footer">Telpon</th>
							<th class="footer">PIC</th>
						</tr>
					</tfoot>
				</table>
                </div>
			</div>
		</div>
	</div>

	<script id="customerjs">
		$(document).ready(function () {
			$('#customer-table tfoot th').each(function () {
				var title = $(this).text();
				var inp = '<input type="text" class="form-control footer-s" id="' + title + '" placeholder="' + title + '" />';
				$(this).html(inp);
			});

			var table = $('#customer-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'master_controller/customer_table',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});

		function destroy(id) {
			swal({
					title: "Apa kamu yakin ?",
					text: "Data akan dihapus secara permanen.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Hapus Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/customer/id_customer/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('master_controller/customer', '.content');
    							swal("Success", "Berhasil menghapus data customer.", "success");
							}else{
								swal("Oops", "Hapus gagal.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

        function edit(id)
        {
            loadView('master_controller/form/customer/'+id+'/null','.content');
		}
		
		document.getElementById('customerjs').innerHTML = "";
		
	</script>

	<style>
		#customer-table_filter{
			display:none;
		}
	</style>
