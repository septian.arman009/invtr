<div class="container-fluid">
	<div class="block-header">
		<h2>CUSTOMER</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('master_controller/customer','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						<?php 
                            if($id == 'null') {
                                echo 'Tambah Customer';
                            }else{
                                echo 'Edit Customer';
                            }
                        ?>
					</h2>
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">

							<div class="form-group">
								<label>Kode Customer</label>
								<div class="form-line">
									<input readonly type="text" value="<?php echo $kd_customer ?>" class="form-control"
										id="kd_customer" />
								</div>
							</div>
							<div class="form-group">
								<label>Token</label>
								<div class="form-line">
									<input readonly type="text" value="<?php echo $token ?>" class="form-control"
										id="token" />
								</div>
							</div>
							<div class="form-group">
								<label>Nama</label>
								<div class="form-line">
									<input onkeyup="form_check($(this).val())" type="text" class="form-control"
										id="nama" />
								</div>
							</div>

                            <div class="form-group">
								<label>Alamat</label>
								<div class="form-line">
									<textarea onkeyup="form_check($(this).val())" class="form-control"
										id="alamat"></textarea> 
								</div>
							</div>

                            <div class="form-group">
								<label>No. Telpon</label>
								<div class="form-line">
									<input type="number" onkeyup="form_check($(this).val())" class="form-control"
										id="tlp"> 
								</div>
							</div>

                            <div class="form-group">
								<label>PIC</label>
								<div class="form-line">
									<input type="text" onkeyup="form_check($(this).val())" class="form-control"
										id="pic"> 
								</div>
							</div>

						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
									<button disabled id="save" onclick="action()"
										class="btn btn-block bg-pink waves-effect">Simpan</button>
									<button style="display:none" id="loading"
										class="btn btn-block bg-pink waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="customerformjs">
	var id = '<?php echo $id ?>';

	get_data(id);

	function get_data(id) {
		if (id != 'null') {
			data = {
				id: id,
				column: 'id_customer',
				table: 'customer'
			}

			postData('main_controller/get_data', data, function (err, response) {
				if (response) {
					if (response.status == 'success') {
						$("#kd_customer").val(response.data[0].kd_customer);
						$("#nama").val(response.data[0].nama);
						$("#alamat").val(response.data[0].alamat);
                        $("#tlp").val(response.data[0].tlp);
                        $("#pic").val(response.data[0].pic);
						setTimeout(() => {
							form_check();
						}, 100);
					}
				} else {
					console.log('ini error : ', err);
				}
			});
		}
	}

	function form_check() {
		if ($("#nama").val() != '' && $("#alamat").val() != '' 
            && $("#tlp").val() != '' && $("#pic").val() != '') {

			$("#save").removeAttr('disabled');
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}

	function action() {
		$("#save").hide();
		$("#loading").show();
		var data = {
			id: id,
			table: 'customer',
			kd_customer: $('#kd_customer').val(),
			nama: $('#nama').val(),
			alamat: $('#alamat').val(),
			tlp: $('#tlp').val(),
			pic: $('#pic').val()
		}

		postData('master_controller/customer_action', data, function (err, response) {
			if (response) {
				var status = response.status;
				if (status == 'success') {
					$("#save").show();
					$("#loading").hide();
					$("#nama").val('');
					$("#alamat").val('');
					$("#tlp").val('');
					$("#pic").val('');
					swal("Success", "Berhasil simpan data customer.", "success");
					if (id != 'null') {
						loadView('master_controller/customer', '.content');
					}else{
						loadView("master_controller/form/customer/null/null", ".content");
					}
				} else {
					$("#save").show();
					$("#loading").hide();
					swal("Oops..!", "Simpan data gagal.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('customerformjs').innerHTML = "";
</script>