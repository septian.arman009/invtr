<?php

$this->fpdf->FPDF_17('L', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/images/ziz.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'SIPERANG HAMA', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. xx', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - xx / 08xx', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 28.5, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 28.5, 3.7);

if ($data_transaksi) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, ' Laporan Barang Masuk Bulan '.to_month($month).' Tahun '.$year, 0, 0, 'C');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Kode Transaksi', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Tgl Transaksi', 1, 0, 'C');
    $this->fpdf->Cell(5, 1, 'Daftar Barang', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Harga Satuan', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Dibuat Oleh', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Diupdate Oleh', 1, 0, 'C');
    $this->fpdf->Cell(2.5, 1, 'Total Barang', 1, 0, 'C');
    $this->fpdf->Cell(3, 1, 'Total Harga', 1, 0, 'C');


    $this->fpdf->Ln();

    $no = 1;
    $total = 0;
    $harga_total = 0;
    foreach ($data_transaksi as $key => $value) {
        $list = unserialize($value['daftar_barang']);
        $qty = array_sum(array_column($list, 'jml_barang'));
        $total_harga = array_sum(array_column($list, 'total_harga'));
        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(4, 0.5, $value['kd_brg_masuk'], 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, to_date($value['tgl_masuk']), 1, 0, 'L');
        $this->fpdf->Cell(5, 0.5, "{$list[0]['nama']} : {$list[0]['jml_barang']}", 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, torp($list[0]['total_harga']/$list[0]['jml_barang']), 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_id']), 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, $this->main_model->gdo4p('users', 'name', 'user_id', $value['user_updated']), 1, 0, 'L');
        $this->fpdf->Cell(2.5, 0.5, $qty, 1, 0, 'L');
        $this->fpdf->Cell(3, 0.5, torp($total_harga), 1, 0, 'L');
        $this->fpdf->Ln();
        
        $data = 1;
        foreach ($list as $key => $vl) {
            if($data == 2){
                $this->fpdf->Cell(0.0001, 0.5, '', 1, 0, 'L');
                $this->fpdf->Cell(5, 0.5, "{$vl['nama']} : {$vl['jml_barang']} ({$customer})", 1, 0, 'L');
                $this->fpdf->Cell(3, 0.5, torp($vl['total_harga']/$vl['jml_barang']), 1, 0, 'L');
                $this->fpdf->Cell(11.5, 0.5, '', 0, 0, 'C');
                $this->fpdf->Cell(0.0001, 0.5, '', 1, 0, 'L');
                $this->fpdf->Ln();
            }
            $data++;
        }
        $total += $qty;
        $harga_total += $total_harga;
    }

    $this->fpdf->Cell(8, 0, '', 1, 0, 'C');
    $this->fpdf->Cell(11, 0, '', 1, 0, 'L');
    $this->fpdf->Cell(8.5, 0, '', 1, 0, 'C');
    $this->fpdf->Ln();

    $this->fpdf->Cell(16, 0.5, '', 0, 0, 'L');
    $this->fpdf->Cell(6, 0.5, 'TOTAL', 1, 0, 'L');
    $this->fpdf->Cell(2.5, 0.5, $total, 1, 0, 'L');
    $this->fpdf->Cell(3, 0.5, torp($harga_total), 1, 0, 'L');

    $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
