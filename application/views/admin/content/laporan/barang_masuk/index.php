<div class="container-fluid">
	<div class="block-header">
		<h2>DLAPORAN BARANG MASUK</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<select style="width: 30%" onchange="filter($(this).val())" id="year" class="form-control">
						<?php for ($i = 2019; $i <= 2099; $i++) {?>
						<?php if ($i == $year) {?>
						<option selected value="<?php echo $i ?>">
							<?php echo $i ?>
						</option>
						<?php } else {?>
						<option value="<?php echo $i ?>">
							<?php echo $i ?>
						</option>
						<?php }?>
						<?php }?>
					</select>
				</div>
				<div class="body">
					<div class="row">
						<div class="col-md-12">
							<div id="masukGraph"></div>
						</div>
						<div class="col-md-12">
							<select style="width: 30%" onchange="filter1($(this).val())" id="month"
								class="form-control">
								<?php
								foreach ($month as $key => $value) { 
									if($value == $m){ ?>
								<option selected value="<?php echo to_m($value) ?>">
									<?php echo $value.' '.$year ?></option>
								<?php }else{?>
								<option value="<?php echo to_m($value) ?>"><?php echo $value.' '.$year ?>
								</option>
								<?php } } ?>
							</select>
						</div>
						<div class="col-md-12">
							<a href='laporan_controller/print_all/barang_masuk/<?php echo $m.'/'.$year ?>' target="_blank"
								class="btn btn-primary waves-effect"> Cetak Laporan Bulan <?php echo $m ?> </a>
						</div>
						<div class="col-md-12">
							<table class="table">
								<thead>
									<tr>
										<th id="th">No</th>
										<th id="th">Kode Barang Masuk</th>
										<th id="th">Tanggal Masuk</th>
										<th id="th">Daftar Barang</th>
										<th id="th">Print</th>
									</tr>
								</thead>

								<tbody>
									<?php $no = 1; foreach ($daftar_barang as $key => $value) { 
										$data = unserialize($value['daftar_barang']);
										?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value['kd_brg_masuk'] ?></td>
										<td><?php echo to_date($value['tgl_masuk']) ?></td>
										<td>
											<?php foreach ($data as $key => $vd) {
												$total_harga = torp($vd['total_harga']);
												echo "<p>{$vd['nama']} ({$vd['kd_barang']}) = {$vd['jml_barang']} {$vd['satuan']} ({$total_harga})</p>";
											} ?>
										</td>
										<td>
											<a title="Cetak"
												href="<?php echo base_url() ?>print/barang_masuk/<?php echo $value['kd_brg_masuk']?>"
												target="_blank" class="btn btn-info btn-xs waves-effect">
												<i class="material-icons">print</i>
											</a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script id="laporanmasukjs">
		function filter(year) {
			var month = $("#month").val();
			loadView('laporan_controller/barang_masuk/' + month + '/' + year, '.content');
		}

		function filter1(month) {
			var year = $("#year").val();
			loadView('laporan_controller/barang_masuk/' + month + '/' + year, '.content');
		}

		$(function () {
			var chart;
			$(document).ready(function () {
				$.getJSON("laporan_controller/masukGraph/<?php echo $year ?>", function (json) {

					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'masukGraph',
							type: 'line'

						},
						title: {
							text: 'Statistic Barang Masuk <?php echo $year ?>'

						},
						subtitle: {
							text: ''

						},
						credits: {
							enabled: false
						},
						xAxis: {
							categories: <?php echo json_encode($indoMonth) ?>
						},
						yAxis: {
							title: {
								text: 'Jumlah Barang'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.series.name + '</b><br/>' +
									this.x + ': ' + this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});

			});

		});

		document.getElementById('laporanmasukjs').innerHTML = "";
	</script>