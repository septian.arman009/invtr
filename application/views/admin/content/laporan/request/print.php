<?php

$this->fpdf->FPDF_17('L', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/images/ziz.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'SIPERANG HAMA', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. xx', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - xx / 08xx', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 28.5, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 28.5, 3.7);

if ($daftar_barang) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, ' Kode Request : '.$data_transaksi[0]['kd_request'], 0, 0, 'C');

    $this->fpdf->SetFont('Times', '', 11);
    $this->fpdf->Ln(1);
    $this->fpdf->Cell(3, 0, 'Dibuat Oleh', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, $this->main_model->gdo4p('users', 'name', 'user_id', $data_transaksi[0]['user_id']), 0, 0, 'L');
    $this->fpdf->Ln(0.5);    
    $this->fpdf->Cell(3, 0, 'Dikonfirmasi Oleh', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, $this->main_model->gdo4p('users', 'name', 'user_id', $data_transaksi[0]['user_accepted']), 0, 0, 'L');
    $this->fpdf->Ln(0.5);    
    $this->fpdf->Cell(3, 0, 'Tanggal Request', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, ':', 0, 0, 'L');
    $this->fpdf->Cell(0.5, 0, to_date($data_transaksi[0]['tgl_request'])  , 0, 0, 'L');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(4.5, 1, 'Kode Barang', 1, 0, 'C');
    $this->fpdf->Cell(7.5, 1, 'Suplier', 1, 0, 'C');
    $this->fpdf->Cell(8.5, 1, 'Nama Barang', 1, 0, 'C');
    $this->fpdf->Cell(2, 1, 'Jumlah', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Status', 1, 0, 'C');

    $this->fpdf->Ln();

    if($data_transaksi[0]['status_terima'] == 0){
        $status_terima = 'Menunggu Konfirmasi';
    }else if($data_transaksi[0]['status_terima'] == 1){
        $status_terima = 'Request Diterima';
    }else if($data_transaksi[0]['status_terima'] == 2){
        $status_terima = 'Request Ditolak';
    }

    $no = 1;
    $total = 0;
    $total_harga = 0;
    foreach ($daftar_barang as $key => $value) {

        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(4.5, 0.5, $value['kd_barang'], 1, 0, 'L');
        $this->fpdf->Cell(7.5, 0.5, $this->main_model->gdo4p('suplier', 'nama', 'kd_suplier', $value['kd_suplier']), 1, 0, 'L');
        $this->fpdf->Cell(8.5, 0.5, $value['nama'], 1, 0, 'L');
        $this->fpdf->Cell(2, 0.5, "{$value['jml_barang']} {$value['satuan']}", 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, $status_terima, 1, 0, 'L');
        $this->fpdf->Ln();
        $total += $value['jml_barang'];
    }

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->Cell(1, 0.5, "",0, 0, 'C');
    $this->fpdf->Cell(4.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(7.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(8.5, 0.5, "Total", 1, 0, 'C');
    $this->fpdf->Cell(2, 0.5, $total, 1, 0, 'L');
    $this->fpdf->Cell(4, 0.5,'', 1, 0, 'L');

    $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
