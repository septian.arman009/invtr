<div class="container-fluid">
	<div class="block-header">
		<h2>DLAPORAN REQUEST</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
				<select style="width: 30%" onchange="filter($(this).val())" id="year" class="form-control">
					<?php for ($i = 2019; $i <= 2099; $i++) {?>
					<?php if ($i == $year) {?>
					<option selected value="<?php echo $i ?>">
						<?php echo $i ?>
					</option>
					<?php } else {?>
					<option value="<?php echo $i ?>">
						<?php echo $i ?>
					</option>
					<?php }?>
					<?php }?>
				</select>
				</div>
				<div class="body">
					<div class="row">
						<div class="col-md-12">
							<div id="requestGraph"></div>
						</div>
						<div class="col-md-12">
							<select style="width: 30%" onchange="filter1($(this).val())" id="month" class="form-control">
								<?php
								foreach ($month as $key => $value) { 
									if($value == $m){ ?>
								<option selected value="<?php echo to_m($value) ?>">
									<?php echo $value.' '.$year ?></option>
								<?php }else{?>
								<option value="<?php echo to_m($value) ?>"><?php echo $value.' '.$year ?>
								</option>
								<?php } } ?>
							</select>
						</div>
						<div class="col-md-12">
							<table class="table">
								<thead>
									<tr>
										<th id="th">No</th>
										<th id="th">Kode Request</th>
										<th id="th">Tanggal Request</th>
										<th id="th">Daftar Barang</th>
                                        <th id="th">Status</th>
										<th id="th">Print</th>
									</tr>
								</thead>

								<tbody>
									<?php $no = 1; foreach ($daftar_barang as $key => $value) { 
										$data = unserialize($value['daftar_barang']);
										?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $value['kd_request'] ?></td>
										<td><?php echo to_date($value['tgl_request']) ?></td>
										<td>
											<?php foreach ($data as $key => $vd) {
												echo "<p>{$vd['nama']} ({$vd['kd_barang']}) -> {$vd['jml_barang']} {$vd['satuan']}</p>";
											} ?>
										</td>
                                        <td>
                                        <?php 
                                        if($value['status_terima'] == 0){ 
                                            echo "<p style='color: orange; font-weight: bold'>Menunggu Konfirmasi</p>";
                                        }else if($value['status_terima'] == 1){ 
                                            echo "<p style='color: green'; font-weight: bold>Request Diterima</p>";
                                        }else if($value['status_terima'] == 2){
                                            echo "<p style='color: red'; font-weight: bold>Request Ditolak</p>";
                                        } ?>
                                        </td>
										<td>
											<a title="Cetak" href="<?php echo base_url() ?>print/request/<?php echo $value['kd_request']?>" target="_blank" class="btn btn-info btn-xs waves-effect">
												<i class="material-icons">print</i>
											</a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script id="laporanrequestjs">
		function filter(year) {
			var month = $("#month").val();
			loadView('laporan_controller/request/' + month + '/' + year, '.content');
		}

		function filter1(month) {
			var year = $("#year").val();
			loadView('laporan_controller/request/' + month + '/' + year, '.content');
		}

		$(function () {
			var chart;
			$(document).ready(function () {
				$.getJSON("laporan_controller/requestGraph/<?php echo $year ?>", function (json) {

					chart = new Highcharts.Chart({
						chart: {
							renderTo: 'requestGraph',
							type: 'bar'

						},
						title: {
							text: 'Statistic Barang request <?php echo $year ?>'

						},
						subtitle: {
							text: ''

						},
						credits: {
							enabled: false
						},
						xAxis: {
							categories: <?php echo json_encode($indoMonth) ?>
						},
						yAxis: {
							title: {
								text: 'Jumlah Barang'
							},
							plotLines: [{
								value: 0,
								width: 1,
								color: '#808080'
							}]
						},
						tooltip: {
							formatter: function () {
								return '<b>' + this.series.name + '</b><br/>' +
									this.x + ': ' + this.y;
							}
						},
						legend: {
							layout: 'vertical',
							align: 'right',
							verticalAlign: 'top',
							x: -10,
							y: 120,
							borderWidth: 0
						},
						series: json
					});
				});

			});

		});

		document.getElementById('laporanrequestjs').innerHTML = "";
	</script>