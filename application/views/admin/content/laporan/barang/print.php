<?php

$this->fpdf->FPDF_17('L', 'cm', 'A4');
$this->fpdf->AliasNbPages();
$this->fpdf->AddPage();

$this->fpdf->Image('assets/images/ziz.png', 1, 1, 2.5, 0, '', base_url('admin'));

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 10);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, '', 0, 0, 'C');

$this->fpdf->SetFont('Times', 'B', 24);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'SIPERANG HAMA', 0, 0, 'C');

$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.8);
$this->fpdf->Cell(0, 0, 'Jl. xx', 0, 0, 'C');
$this->fpdf->SetFont('Times', '', 12);
$this->fpdf->Ln(0.5);
$this->fpdf->Cell(0, 0, 'Telp. 021 - xx / 08xx', 0, 0, 'C');

$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.6, 28.5, 3.6);
$this->fpdf->Ln(0.5);
$this->fpdf->Line(1, 3.7, 28.5, 3.7);

if ($barang) {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, 'Daftar Barang', 0, 0, 'C');

    $this->fpdf->SetFont('Times', 'B', 10);
    $this->fpdf->ln(1);
    $this->fpdf->Cell(1, 1, 'No', 1, 0, 'C');
    $this->fpdf->Cell(4.5, 1, 'Kode Barang', 1, 0, 'C');
    $this->fpdf->Cell(6.5, 1, 'Suplier', 1, 0, 'C');
    $this->fpdf->Cell(7.5, 1, 'Nama Barang', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Jumlah', 1, 0, 'C');
    $this->fpdf->Cell(4, 1, 'Harga', 1, 0, 'C');

    $this->fpdf->Ln();

    $no = 1;
    $total = 0;
    $total_harga = 0;
    foreach ($barang as $key => $value) {

        $this->fpdf->SetFont('Times', '', 11);
        $this->fpdf->Cell(1, 0.5, $no++, 1, 0, 'C');
        $this->fpdf->Cell(4.5, 0.5, $value['kd_barang'], 1, 0, 'L');
        $this->fpdf->Cell(6.5, 0.5, $this->main_model->gdo4p('suplier', 'nama', 'kd_suplier', $value['kd_suplier']), 1, 0, 'L');
        $this->fpdf->Cell(7.5, 0.5, $value['nama'], 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, "{$value['jml_barang']} {$value['satuan']}", 1, 0, 'L');
        $this->fpdf->Cell(4, 0.5, torp($value['harga']), 1, 0, 'L');
        $this->fpdf->Ln();
        $total += $value['jml_barang'];
        $total_harga += $value['harga'] * $value['jml_barang'];
    }

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->Cell(1, 0.5, "",0, 0, 'C');
    $this->fpdf->Cell(4.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(6.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(7.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(4, 0.5, 'Total Barang', 1, 0, 'L');
    $this->fpdf->Cell(4, 0.5, $total, 1, 0, 'L');

    $this->fpdf->Ln();

    $this->fpdf->SetFont('Times', 'B', 11);
    $this->fpdf->Cell(1, 0.5, "",0, 0, 'C');
    $this->fpdf->Cell(4.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(6.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(7.5, 0.5, "", 0, 0, 'C');
    $this->fpdf->Cell(4, 0.5,'Total Asset', 1, 0, 'L');
    $this->fpdf->Cell(4, 0.5, torp($total_harga), 1, 0, 'L');

    $this->fpdf->Ln();
} else {
    $this->fpdf->SetFont('Times', 'B', 12);
    $this->fpdf->Ln(0.6);
    $this->fpdf->Cell(0, 0, "No data to show.", 0, 0, 'C');
}

$this->fpdf->Ln();

$this->fpdf->Output();
