<div class="container-fluid">
	<div class="block-header">
		<h2>DAFTAR REQUEST</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<?php if(role(['user'], false)){ ?>
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('request_controller/request/null','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						Tambah Request
					</h2>
				</div>
				<?php } ?>
				<div class="body table-responsive">
					<table id="request-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Kode Request</th>
								<th id="th">Tangagl Request</th>
								<th id="th">Dibuat Oleh</th>
								<th id="th">Diterima/tolak Oleh</th>
								<th id="th">Jumlah Barang</th>
								<th id="th" class="no-sort" width="15%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Kode Request</th>
								<th class="footer">DD-MM-YYYY</th>
								<th class="footer">Dibuat Oleh</th>
								<th class="footer">Diupdate Oleh</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script id="brgrequestjs">
		$(document).ready(function () {
			$('#request-table tfoot th').each(function () {
				var title = $(this).text();
				if (title == 'DD-MM-YYYY') {
					var inp = '<input readonly style="cursor: pointer; color: black;" type="text" class="form-control footer-s datepicker" placeholder="' + title + '" id="' +
					title + ' data-date="<?php echo date("Y-m-d") ?>" />';
				} else {
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
				}
				$(this).html(inp);
			});

			var table = $('#request-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'request_controller/request_table',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});
		
		$('.datepicker').bootstrapMaterialDatePicker({
            format: 'DD MMMM YYYY',
            clearButton: false,
            weekStart: 1,
            time: false
        });

		function edit(id) {
			loadView('request_controller/request/' + id, '.content');
		}

		function accept(id) {
			swal({
					title: "Terima Request ?",
					text: "Menerima request akan mengurangi stock pada tabel barang.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Terima Request",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('request_controller/accept', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('request_controller/daftar_request', '.content');
								swal("Success", "Berhasil menerima request.", "success");
							} else {
								swal("Oops", "User harus membuat request baru.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);;
		}

		function refuse(id) {
			swal({
					title: "Tolak Request ?",
					text: "Menolak request akan mengemabalikan jumlah barang di tabel barang, sebelum request user.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Tolak Request",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('request_controller/refuse', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('request_controller/daftar_request', '.content');
								swal("Success", "Berhasil menolak request.", "success");
							} else {
								swal("Oops", "Menolak request gagal.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		document.getElementById('brgrequestjs').innerHTML = "";
	</script>

	<style>
		#request-table_filter {
			display: none;
		}
	</style>