<div class="container-fluid">
	<div class="block-header">
		<h2>BARANG KELUAR</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<?php if($id != 'null'){ ?>
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('transaksi_controller/daftar_brg_keluar','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						Edit Barang Keluar
					</h2>
					<?php }else{?>
					<h2>
						<a class="btn btn-primary waves-effect pull-right" style="cursor:pointer"
							onclick="loadView('transaksi_controller/daftar_brg_keluar','.content')">
							Daftar Barang Keluar
						</a>
					</h2>
					Form Barang Keluar
					<?php } ?>
					
				</div>
				<div class="body">
					<div class="row clearfix">
						<div class="col-sm-12">

                            <div class="form-group">
								<label>Kode Barang Keluar</label>
								<div class="form-line">
									<input readonly id="kd_brg_keluar" type="text" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label>Nama Barang</label>
								<div class="form-line">
									<input onchange="getBarang($(this).val())" id="nama_barang" type="text"
										class="typehead form-control">
								</div>
							</div>

							<div class="card">
								<div class="body">
									<table class="table">
										<thead>
											<tr>
												<th>Detail Barang</th>
											</tr>
										</thead>

										<tbody id="tbody">
											<tr>
												<td>Kode Barang</td>
												<td>:</td>
												<td>...</td>
											</tr>
											<tr>
												<td>Nama Barang</td>
												<td>:</td>
												<td>...</td>
											</tr>
											<tr>
												<td>Satuan</td>
												<td>:</td>
												<td>...</td>
											</tr>
											<tr>
												<td>Stock Barang</td>
												<td>:</td>
												<td>...</td>
											</tr>
											<tr>
												<td>Harga</td>
												<td>:</td>
												<td>...</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

                            <div class="form-group">
								<label>Customer</label>
								<div class="form-line">
									<input onchange="getCustomer($(this).val())" id="kd_customer" type="text" class="typehead form-control">
								</div>
							</div>

							<div class="form-group">
								<label>Jumlah Barang</label>
								<div class="form-line">
									<input onkeyup="check_form()" id="jml_barang" type="number"
										class="form-control">
								</div>
							</div>

							<div class="form-group">
								<label>Tanggal keluar</label>
								<div class="form-line">
									<input type="text" class="pointer datepicker form-control" id="tgl_keluar"
										value="<?php echo $date ?>">
								</div>
							</div>

							<div class="form-group">
								<label>Total</label>
								<div class="form-line">
									<input readonly id="total" value="Rp. 0" type="text" class="form-control">
								</div>
							</div>

						</div>

                        <div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
								    <button disabled id="tambah" onclick="tambahBarang()" class="btn btn-block bg-green waves-effect">Tambah Barang</button>
									<button style="display:none" id="loadingTambah" class="btn btn-block bg-green waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

						<div class="col-md-2">
							<div class="form-group">
								<div class="form-line">
								    <button disabled id="simpan" onclick="simpanTransaksi()" class="btn btn-block bg-pink waves-effect">Simpan</button>
									<button style="display:none" id="loadingSimpan" class="btn btn-block bg-pink waves-effect">Mohon Tunggu ..</button>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<table class="table stripe hover">
								<thead>
									<tr>
										<th id="th">No</th>
										<th id="th">Kode Barang keluar</th>
										<th id="th">Kode Barang</th>
										<th id="th">Nama Barang</th>
										<th id="th">Customer</th>
                                        <?php if($id == 'null'){ ?>
                                            <th id="th">Stock Saat Ini</th>
                                        <?php }else{?>
                                            <th id="th">Stock Awal</th>
                                        <?php }?>
                                        <th id="th">Barang keluar</th>
                                        <th id="th">Total Stock</th>
                                        <th id="th"></th>
									</tr>
								</thead>
								<tbody id="tbody">
                                <?php $no = 1; foreach ($daftar_barang as $key => $value) { 
									$stock = $this->main_model->gdo4p('barang', 'jml_barang', 'kd_barang', $value['kd_barang']);
									$customer = $this->main_model->gdo4p('customer', 'nama', 'kd_customer', $value['kd_customer']);
									?>
									<tr>
										<td><?php echo $no++ ?></td>
										<td><?php echo $kd_brg_keluar ?></td>
										<td><?php echo $value['kd_barang'] ?></td>
										<td><?php echo $value['nama'] ?></td>
										<td><?php echo "{$customer} <p>{$value['kd_customer']}</p>" ?></td>
										<?php if($id == 'null'){ ?>
											<td><?php echo "{$stock} {$value['satuan']}" ?></td>
											<td><?php $total_harga = torp($value['total_harga']); echo "{$value['jml_barang']} {$value['satuan']} <p>{$total_harga}</p>" ?></td>
											<td><?php echo ($stock - $value['jml_barang']) .' '.$value['satuan'] ?><p style="color: red;">Jumlah setelah disimpan.</p></td>
										<?php }else{ ?>
											<?php if($value['status'] == 0) {?>
												<td><?php echo $stock + $value['jml_barang'] ?></td>
												<td><?php $total_harga = torp($value['total_harga']); echo "{$value['jml_barang']} {$value['satuan']} <p>{$total_harga}</p>" ?></td>
												<td><?php echo $stock ?></td>
											<?php }else{ ?>
												<td><?php echo "{$stock} {$value['satuan']}" ?></td>
												<td><?php $total_harga = torp($value['total_harga']); echo "{$value['jml_barang']} {$value['satuan']} <p>{$total_harga}</p>" ?></td>
												<td><?php echo ($stock - $value['jml_barang']) .' '.$value['satuan'] ?><p style="color: red;">Jumlah setelah disimpan.</p></td>
											<?php } ?>
										<?php } ?>
										<td>
                                            <a title="Delete" class="btn btn-danger btn-xs waves-effect" onclick="hapus_barang('<?php echo $value['kd_barang'] ?>', '<?php echo $value['kd_customer'] ?>');"><i class="material-icons">delete</i></a>
										</td>
									</tr>
								<?php } ?>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script id="brkeluarformjs">
	$('#nama_barang').typeahead({
		source: function (query, process) {
			return $.get('transaksi_controller/cari_barang', {
				query: query
			}, function (data) {
				data = $.parseJSON(data);
				return process(data);
			});
		}
    });

    $('#kd_customer').typeahead({
		source: function (query, process) {
			return $.get('transaksi_controller/cari_customer', {
				query: query
			}, function (data) {
				data = $.parseJSON(data);
				return process(data);
			});
		}
    });

    var status = '<?php echo $status ?>';
    var id = '<?php echo $id ?>';

    if(status == 0){
        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'DD MMMM YYYY',
            clearButton: false,
            weekStart: 1,
            time: false
        });
        generate_code();
    }else{
        $("#kd_brg_keluar").val('<?php echo $kd_brg_keluar ?>');
        $("#simpan").removeAttr("disabled");
    }

    function check_form() {
		if(this.kd_barang != 'false' && this.harga != 'false' && this.kd_customer != 'false'){
			var jml = $("#jml_barang").val();
            if(jml != ''){
                $("#total").val(to_rp(jml * this.harga));
                $("#tambah").removeAttr("disabled");
            }else{
                $("#total").val('Rp. 0');
                $("#tambah").attr("disabled", "disabled");
            }
		}else{
			$("#tambah").attr("disabled", "disabled");
		}
    }

    function getBarang(nama) {
		data = {
			id: nama,
			column: 'nama',
			table: 'barang'
		}

		postData('main_controller/get_data', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#tbody").html(
                        '<tr><td>Kode Barang</td><td>:</td><td>'+response.data[0].kd_barang+'</td></tr>'+
                        '<tr><td>Nama Barang</td><td>:</td><td>'+response.data[0].nama+'</td></tr>'+
                        '<tr><td>Satuan</td><td>:</td><td>'+response.data[0].satuan+'</td></tr>'+
						'<tr><td>Stock Barang</td><td>:</td><td>'+response.data[0].jml_barang+'</td></tr>'+
                        '<tr><td>Harga</td><td>:</td><td>'+to_rp(response.data[0].harga)+'</td></tr>'
					);
                    this.kd_barang = response.data[0].kd_barang;
					this.harga = response.data[0].harga;
                    
					setTimeout(() => {
						check_form();
					}, 100);
				}else{
					$("#tbody").html(
						'<tr><td>Kode Barang</td><td>:</td>...<td></td></tr>'+
                        '<tr><td>Nama Barang</td><td>:</td>...<td></td></tr>'+
                        '<tr><td>Satuan</td><td>:</td><td>...</td></tr>'+
						'<tr><td>Stock Barang</td><td>:</td><td>...</td></tr>'+
                        '<tr><td>Harga</td><td>:</td><td>...</td></tr>'
					);
                    this.kd_barang = 'false';
                    this.harga = 'false';
					setTimeout(() => {
						check_form();
					}, 100);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
    }

    function getCustomer(customer) {
		data = {
			id: customer,
			column: 'nama',
			table: 'customer'
		}

		postData('main_controller/get_data', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					this.kd_customer = response.data[0].kd_customer;
					setTimeout(() => {
						check_form();
					}, 100);
				}else{
					this.kd_customer = 'false';
					setTimeout(() => {
						check_form();
					}, 100);
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

    function tambahBarang() {
		$("#tambah").hide();
		$("#loadingTambah").show();
		data = {
			kd_transaksi: $("#kd_brg_keluar").val(),
			tbl_transaksi: 'barang_keluar',
			klm_transaksi: 'kd_brg_keluar',
            kd_customer: this.kd_customer,
			kd_barang: this.kd_barang,
            jml_barang: $("#jml_barang").val(),
			total_harga: $("#total").val(),            
			tgl_transaksi: $("#tgl_keluar").val()
		}

		postData('transaksi_controller/tambahBarang/<?php echo $id ?>', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#tambah").show();
					$("#loadingTambah").hide();
					swal("Sukses", "Data barang berhasil disimpan di daftar barang keluar dengan nomor transaksi : "+$("#kd_brg_keluar").val(), "success");
					loadView('transaksi_controller/barang_keluar/<?php echo $id ?>', '.content');
				}else{
					$("#tambah").show();
					$("#loadingTambah").hide();
					swal("Terjadi Kesalahan", "Barang sudah didaftarkan, silakan hapus barang terlebih dahulu.", "error");
				}
			} else {
				console.log('ini error : ', err);
			}
		});
    }
    
    function generate_code() {
		var date = $("#tgl_keluar").val();
        var name_code = '';
        
        var array_date = date.split(' ');
        if(array_date[1] == 'January'){
            var month = '01';
        }else if(array_date[1] == 'February'){
            var month = '02';
        }else if(array_date[1] == 'March'){
            var month = '03';
        }else if(array_date[1] == 'April'){
            var month = '04';
        }else if(array_date[1] == 'May'){
            var month = '05';
        }else if(array_date[1] == 'June'){
            var month = '06';
        }else if(array_date[1] == 'July'){
            var month = '07';
        }else if(array_date[1] == 'August'){
            var month = '08';
        }else if(array_date[1] == 'September'){
            var month = '09';
        }else if(array_date[1] == 'October'){
            var month = '10';
        }else if(array_date[1] == 'November'){
            var month = '11';
        }else if(array_date[1] == 'December'){
            var month = '12';
        }		
		var middle_code = array_date[0]+month+''+array_date[2].slice(2,4);
			
		var kd_brg_keluar = 'KL'+middle_code+'<?php echo $kd_brg_keluar ?>';
		
		$("#kd_brg_keluar").val(kd_brg_keluar);
	}
    
    function hapus_barang(kd_barang, kd_customer) {
		swal({
				title: "Apakah anda yakin ?",
				text: "Hapus barang : "+kd_barang+" ?",
				type: "warning",
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Yes, Hapus Sekarang",
				closeOnConfirm: false
			},
			function () {
				var data = {
					tbl_transaksi: 'barang_keluar',
					klm_transaksi: 'kd_brg_keluar',
					kd_transaksi: $("#kd_brg_keluar").val(),
					kd_barang: kd_barang,
                    kd_customer: kd_customer
				}
				postData('transaksi_controller/hapusBarang/<?php echo $id ?>', data, function (err, response) {
					if (response) {
						if (response.status == 'success') {
							swal("Sukses", "Berhasil menghapus data barang dari daftar barang.", "success");
							loadView('transaksi_controller/barang_keluar/<?php echo $id ?>', '.content');
						}else{
							swal("Terjadi Kesalahan", "Minimal 1 barang keluar pada daftar barang.", "error");
						}
					} else {
						console.log('ini error : ', err);
					}
				});
			}
		);
	}

	function simpanTransaksi() {
		$("#simpan").hide();
		$("#loadingSimpan").show();
		data = {
			tbl_transaksi: 'barang_keluar',
			klm_transaksi: 'kd_brg_keluar',
			kd_transaksi: $("#kd_brg_keluar").val()
		}

		postData('transaksi_controller/simpanTransaksi/<?php echo $id ?>', data, function (err, response) {
			if (response) {
				if (response.status == 'success') {
					$("#simpan").show();
					$("#loadingSimpan").hide();
					swal("Sukses", "Barang keluar berhasil disimpan dengan nomor transaksi : "+$("#kd_brg_keluar").val(), "success");
					loadView('transaksi_controller/barang_keluar/<?php echo $id ?>', '.content');
				}
			} else {
				console.log('ini error : ', err);
			}
		});
	}

	document.getElementById('brkeluarformjs').innerHTML = "";
</script>