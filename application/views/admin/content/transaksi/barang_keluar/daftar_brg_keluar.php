<div class="container-fluid">
	<div class="block-header">
		<h2>DAFTAR BARANG KELUAR</h2>
	</div>
	<!-- Basic Table -->
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="card">
				<div class="header">
					<h2>
						<a class="btn btn-primary waves-effect" style="cursor:pointer"
							onclick="loadView('transaksi_controller/barang_keluar/null','.content')">
							<i class="material-icons">keyboard_backspace</i>
						</a>
						Tambah Barang Keluar
					</h2>
				</div>
				<div class="body table-responsive">
					<table id="brgKeluar-table" class="table stripe hover">
						<thead>
							<tr>
								<th id="th" width="10%">No</th>
								<th id="th">Kode Barang Keluar</th>
								<th id="th">Tangagl Keluar</th>
								<th id="th">Dibuat Oleh</th>
								<th id="th">Diupdate Oleh</th>
								<th id="th">Nominal Transaksi</th>
								<th id="th" class="no-sort" width="10%">Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th class="footer">no</th>
								<th class="footer">Kode Keluar</th>
								<th class="footer">DD-MM-YYYY</th>
								<th class="footer">Dibuat Oleh</th>
								<th class="footer">Diupdate Oleh</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script id="brgKeluarjs">
		$(document).ready(function () {
			$('#brgKeluar-table tfoot th').each(function () {
				var title = $(this).text();
				if (title == 'DD-MM-YYYY') {
					var inp = '<input readonly style="cursor: pointer; color: black;" type="text" class="form-control footer-s datepicker" placeholder="' + title + '" id="' +
					title + ' data-date="<?php echo date("Y-m-d") ?>" />';
				} else {
					var inp = '<input type="text" class="form-control footer-s" placeholder="' + title + '" id="' + title + '" />';
				}
				$(this).html(inp);
			});

			var table = $('#brgKeluar-table').DataTable({
				"processing": true,
				"serverSide": true,
				"ajax": {
					"url": 'transaksi_controller/brgKeluar_table',
					"type": "POST"
				}
			});

			table.columns().every(function () {
				var that = this;
				$('input', this.footer()).on('keyup change', function () {
					if (that.search() !== this.value) {
						that.search(this.value).draw();
					}
				});
			});
			$("#no").hide();
		});

		$('.datepicker').bootstrapMaterialDatePicker({
            format: 'DD MMMM YYYY',
            clearButton: false,
            weekStart: 1,
            time: false
        });

		function destroy(id) {
			swal({
					title: "Apa kamu yakin ?",
					text: "Data akan dihapus secara permanen.",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Yes, Hapus Now",
					closeOnConfirm: false
				},
				function () {
					var data = {
						id: id
					}
					postData('main_controller/destroy/barang_keluar/id_brg_keluar/', data, function (err, response) {
						if (response) {
							console.log('berhasil : ', response);
							if (response.status == 'success') {
								loadView('transaksi_controller/daftar_brg_keluar', '.content');
								swal("Success", "Berhasil menghapus data barang keluar.", "success");
							} else {
								swal("Oops", "Hapus gagal.", "error");
							}
						} else {
							console.log('ini error : ', err);
						}
					});
				}
			);
		}

		function edit(id) {
			loadView('transaksi_controller/barang_keluar/' + id, '.content');
		}

		document.getElementById('brgKeluarjs').innerHTML = "";
	</script>

	<style>
		#brgKeluar-table_filter {
			display: none;
		}
	</style>