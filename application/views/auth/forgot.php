<?php include 'layout/header.php';?>

<body class="fp-page" style="background: #009688">
	<div class="fp-box">
		<div class="logo">
			<a href="javascript:void(0);">Inventory
				<b>System</b>
			</a>
			<small>Versi 1.0.0</small>
		</div>
		<div class="card">
			<div class="body">
				<div class="msg">
					Masukan email kamu dan kami akan mengirimkan Link Reset Password ke email kamu.
				</div>
				<div class="input-group">
					<span class="input-group-addon">
						<i class="material-icons">email</i>
					</span>
					<div class="form-line">
						<input onkeyup="f_check()" type="email" class="form-control" id="email" placeholder="Email"
							required autofocus>
					</div>
					<div style="display:none;color:red" id="invalid_email">Please type right format of email.</div>
					<div style="display:none;color:red" id="invalid">Email not found.</div>
				</div>

				<button disabled id="send_mail" onclick="send_token()"
					class="btn btn-block btn-lg bg-pink waves-effect">Kirim Link</button>
				<button style="display:none" id="loading" class="btn btn-block btn-lg bg-pink waves-effect">Mohon Tunggu..</button>
				<div class="row m-t-20 m-b--5 align-center">
					<a href="<?php echo base_url('signin') ?>">Masuk</a>
				</div>
			</div>
		</div>
	</div>
	<?php include 'layout/footer.php';?>