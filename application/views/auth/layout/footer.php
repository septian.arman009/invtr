<script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/node-waves/waves.js"></script>
<script src="<?php echo base_url() ?>assets/js/admin.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/main.js"></script>
<script src="<?php echo base_url() ?>assets/myjs/auth.js"></script>
</body>

</html>
