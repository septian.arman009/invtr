<html>

<body>

	<div class="row">
		<div class="col-md-4">
			<p>Bekasi, <?php echo $tanggal ?></p>
			<p>Kepada Yth,</p>
			<p>Orang Tua Dari : <?php echo $murid[0]['nama_lengkap'] ?></p>
			<p>Kelas : <?php echo $murid[0]['kelas'] ?> Sekolah : <?php echo $sekolah ?></p>
			<p>Pengguna Jasa Antar Jemput Kopkar Al Muhajirien Jakapermai</p>
			<p>Di Tempat</p>
			<p><i><b>Assalamu'alaikum Wr. Wb.</b></i></p>
			<p>Puji syukur marilah kita panjatkan kehadirat Allah SWT. Shalawat serta salam semoga selalu tercurah kepada
				Nabi Muhammad SAW beserta keluarga, sahabat dan para pengikutnya yang tetap setia hingga akhir zaman, Amin.</p>
			<p>Perlu kami sampaikan berdasarkan data laporan Rekening Bank yang kami terima pertanggal <?php echo $tanggal ?>
			 bahwa putra/putri bapak/ibu belum menyelesaikan pembayaran Antar Jemput sebagai berikut :</p>
		</div>
	</div>
	<table style="border-collapse: collapse;
    border: 1px solid black;
    width: 100%;
    text-align: center;
    margin: auto;">

		<thead>
			<th style="border: 1px solid black;
			background-color: #4caf50;
			color: white;
			padding: 1%;">No</th>
			<th style="border: 1px solid black;
			background-color: #4caf50;
			color: white;
			padding: 1%;">Bulan</th>
			<th style="border: 1px solid black;
			background-color: #4caf50;
			color: white;
			padding: 1%;">Jumlah</th>
		</thead>

		<tbody>
			<?php $index = 0;?>
			<?php $total = 0;foreach ($bulan as $key => $value) {?>
			<tr>
				<td style="border: 1px solid black;
				padding: 1%;">
					<?php echo $index + 1 ?>
				</td>

				<td style="border: 1px solid black;
				padding: 1%;">
					<?php echo strtoupper($value) ?>
				</td>

				<td style="border: 1px solid black;
				padding: 1%;">
					<?php echo $this->mylib->torp($jumlah[$index]) ?>
				</td>
			</tr>

			<?php $total += $jumlah[$index];?>
			<?php $index++;}?>
			<tr>
				<td>
				</td>

				<td>
					TOTAL
				</td>

				<td style="border: 1px solid black;
				padding: 1%;">
					<?php echo $this->mylib->torp($total) ?>
				</td>
			</tr>
		</tbody>
	</table>

	<p>Kami <b>MOHON MAAF</b> dan agar diabaikan, apabila pada saat menerima surat ini ternyata bapak/ibu sudah membayar, namun kami mohon agar dikirimkan <b>Copy</b> pembayarannya ke Koperasi untuk dilakukan <b>KOREKSI</b></p>
	<p>Apabila bapak/ibu belum melunasinya mohon dapat diselesaikan selambat-lambatnya pada tanggal <?php echo $tanggal_maksimal ?></p>
	<p>Perlu kami sampaikan kembali bahwa, pembayaran abodemen antar jemput selambat-lambatnya tanggal 10 (sepuluh) setiap bulannya dan kami tidak menerima pembayaran tunai, pembayaran di setor langsung ke  :</p>
	<?php $no = 1; foreach ($bank_penerima as $key => $value) { ?>
	<p style ="margin-left:1%"><?php echo $no ?>) <b><?php echo $value['nama_bank'] ?></b> <?php echo $value['cabang'] ?></p>
	<p style ="margin-left:5%">No.rek : <b><?php echo $value['rekening'] ?></b> A/n : <b><?php echo $value['an'] ?></b></p>
	<?php $no++; }?>
	<p>Demikian yang dapat kami sampaikan, atas perhatiannya dan kerjasamanya kami ucapkan terima kasih.</p>
	<p><i><b>Wassalamu'alaikum Wr. Wb.</b></i></p>

</body>

</html>
