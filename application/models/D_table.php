<?php
defined('BASEPATH') or exit('No direct script access allowed');

class D_table extends CI_Model
{
    public function Datatables($dt)
    {
        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];

        if($dt['table'] == 'barang_masuk' || $dt['table'] == 'barang_keluar' || $dt['table'] == 'request'){
            $sql = "SELECT {$columns} FROM {$dt['table']} WHERE status = 0";
        }else{
            $sql = "SELECT {$columns} FROM {$dt['table']}";
        }
        
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        $search = $dt['search']['value'];
        $where = '';
        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $where .= $columnd[$i] . ' LIKE "%' . $search . '%"';

                if ($i < $count_c - 1) {
                    if($dt['table'] == 'barang_masuk' || $dt['table'] == 'barang_keluar' || $dt['table'] == 'request'){
                        $where .= ' AND ';
                    }else{
                        $where .= ' OR ';
                    }
                }
            }
        }
        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                if($dt['table'] == 'barang'){
                    if($i == 2){
                        $suplier = $this->db->query('select kd_suplier from suplier where nama like "%' . $searchCol . '%"')->result_array();
                        $in = takeArrayValueToString($suplier, 'kd_suplier');
                        if ($in != '') {
                            $where = $columnd[$i] . ' IN (' . $in . ')';
                        } else {
                            $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                        }
                    }else{
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" '; 
                    }
                }else if($dt['table'] == 'barang_masuk' || $dt['table'] == 'barang_keluar' || $dt['table'] == 'request'){
                    if($i == 2){
                        $where = $columnd[$i] . "='" . to_date_mysql($searchCol) . "'";
                    }else if($i == 3 || $i == 4){
                        $users = $this->db->query('select user_id from users where name like "%' . $searchCol . '%"')->result_array();
                        $in = takeArrayValueToString($users, 'user_id');
                        if ($in != '') {
                            $where = $columnd[$i] . ' IN (' . $in . ')';
                        } else {
                            $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                        }
                    }else{
                        $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" '; 
                    }
                }else{
                    $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                }

                break;
            }
        }
        if ($where != '') {
            if($dt['table'] == 'barang_masuk' || $dt['table'] == 'barang_keluar' || $dt['table'] == 'request'){
                $sql .= " AND " . $where;
            }else{
                $sql .= " WHERE " . $where;
            }
        }
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        $start = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        $list = $this->db->query($sql);
        $option['draw'] = $dt['draw'];
        $option['recordsTotal'] = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data'] = array();

        $no = 1;
        foreach ($list->result_array() as $row => $val) {
            $rows = array();

           
            $id = 0;
            $data = 1;
            foreach ($dt['col-display'] as $key => $kolom) {
                if ($id == 0) {
                    $id = $val[$kolom];
                    $rows[] = $no;
                } else if($dt['table'] == 'barang') {
                    if($data == 3){
                        $rows[] = $this->gdo4p('suplier', 'nama', 'kd_suplier', $val[$kolom]);
                    }else if($data == 5){
                        $satuan = $val[$kolom];
                    }else if($data == 6){
                        $rows[] = torp($val[$kolom]);
                    }else if($data == 7){
                        $rows[] = $val[$kolom]. ' ' .$satuan;
                    }else{
                        $rows[] = $val[$kolom];
                    }
                } else if($dt['table'] == 'barang_masuk' || $dt['table'] == 'barang_keluar' || $dt['table'] == 'request') {
                    if($data == 3){
                        $rows[] = to_engdate($val[$kolom]);
                    }else if($data == 4 || $data == 5){
                        $rows[] = $this->gdo4p('users', 'name', 'user_id', $val[$kolom]);
                    }else if($data == 5){
                        $rows[] = $this->gdo4p('users', 'name', 'user_id', $val[$kolom]);
                    }else if($data == 6){
                        $rows[] = array_sum(array_column(unserialize($val[$kolom]), 'jml_barang'));
                    }else if($data == 7 && $dt['table'] == 'request'){
                        $status_terima = $val[$kolom];
                    }else {
                        $rows[] = $val[$kolom];
                    }
                } else {
                    $rows[] = $val[$kolom];
                }
                $data++;
            
            }

            if($dt['table'] == 'request'){
                if(role(['admin'], false)){
                    if($status_terima == 0){
                        $rows[] =
                        '<a title="Detail" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">remove_red_eye</i></a>
                        | <a title="Terima Request" class="btn btn-success btn-xs waves-effect"
                        onclick="accept(' . "'" . $id . "'" . ');"><i class="material-icons">check</i></a>
                        | <a title="Tolak Request" class="btn btn-danger btn-xs waves-effect"
                        onclick="refuse(' . "'" . $id . "'" . ');"><i class="material-icons">close</i></a>';
                    }else if($status_terima == 1){
                        $rows[] =
                        '<a title="Detail" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">remove_red_eye</i></a>
                        | <a title="Request Diterima" class="btn btn-success btn-xs waves-effect"><i class="material-icons">check</i></a>
                        | <a title="Ubah Status" class="btn btn-warning btn-xs waves-effect"
                        onclick="refuse(' . "'" . $id . "'" . ');"><i class="material-icons">info_outline</i></a>';
                    }else{
                        $rows[] =
                        '<a title="Detail" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">remove_red_eye</i></a>
                        | <a title="Request Ditolak" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">close</i></a>
                        | <a title="Ubah Status" class="btn btn-warning btn-xs waves-effect"
                        onclick="accept(' . "'" . $id . "'" . ');"><i class="material-icons">info_outline</i></a>';
                    }
                }else{
                    if($status_terima == 0){
                        $rows[] =
                        '<a title="Update" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                        | <a title="Menunggu Konfirmasi" class="btn btn-warning btn-xs waves-effect"><i class="material-icons">info_outline</i></a>';
                    }else if($status_terima == 1){
                        $rows[] =
                        '<a title="Update" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                        | <a title="Request Diterima" class="btn btn-success btn-xs waves-effect"><i class="material-icons">check</i></a>';
                    }else{
                        $rows[] =
                        '<a title="Update" class="btn btn-info btn-xs waves-effect"
                        onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                        | <a title="Request Ditolak" class="btn btn-danger btn-xs waves-effect"><i class="material-icons">close</i></a>';
                    }
                   
                }
                
            }else{
                $rows[] =
                '<a title="Update" class="btn btn-info btn-xs waves-effect"
                onclick="edit(' . "'" . $id . "'" . ');""><i class="material-icons">mode_edit</i></a>
                | <a title="Delete" class="btn btn-danger btn-xs waves-effect"
                onclick="destroy(' . "'" . $id . "'" . ');"><i class="material-icons">delete</i></a>';
            }
            

            $no++;
            $option['data'][] = $rows;

        }
        echo json_encode($option);
    }

    public function gdo4p($table, $take, $column, $id)
    {
        $this->db->where($column, $id);
        $query = $this->db->get($table)->result_array();
        if ($query) {
            if ($query[0][$take]) {
                return $query[0][$take];
            }
        }

    }
}
