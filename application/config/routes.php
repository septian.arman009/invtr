<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'auth_controller';
$route['404_override'] = 'main_controller/error_handling';
$route['translate_uri_dashes'] = false;

$route['signin'] = 'auth_controller/index';
$route['forgot_password'] = 'auth_controller/forgot_password';

$route['admin'] = 'main_controller/index';
$route['reset_password/(:any)'] = 'auth_controller/reset_password/$1';
$route['print/(:any)/(:any)'] = 'laporan_controller/print/$1/$2';



