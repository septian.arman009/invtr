<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Request_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function request($id)
    {
        if (role(['user', 'admin'], false)) {
            $data['id'] = $id;
            if ($id == 'null') {
                $status = $this->main_model->limitWhere(0, 1, 'request', 'status', 1, 'id_request');
                if ($status) {
                    $data['kd_request'] = $status[0]['kd_request'];
                    $data['date'] = to_engdate($status[0]['tgl_request']);
                    $data['status'] = 1;
                    $data['daftar_barang'] = unserialize($status[0]['daftar_barang']);
                } else {
                    $data['kd_request'] = generate_code_tnc('request');
                    $data['date'] = to_engdate(date('Y-m-d'));
                    $data['status'] = 0;
                    $data['daftar_barang'] = array();
                }
            } else {
                $request = $this->main_model->gda3p('request', 'id_request', $id);
                $data['kd_request'] = $request[0]['kd_request'];
                $data['date'] = to_engdate($request[0]['tgl_request']);
                $data['status'] = 1;
                $data['daftar_barang'] = unserialize($request[0]['daftar_barang']);
            }
            $this->load->view('admin/content/request/index', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function tambahBarang($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $kd_transaksi = $obj->kd_request;
        $tbl_transaksi = 'request';
        $klm_transaksi = 'kd_request';
        $kd_barang = $obj->kd_barang;
        $jml_barang = $obj->jml_barang;
        $tgl_transaksi = to_date_mysql($obj->tgl_request);

        $check = $this->main_model->gda3p($tbl_transaksi, $klm_transaksi, $kd_transaksi);
        $barang = $this->main_model->gda3p('barang', 'kd_barang', $kd_barang);
        if (!$check) {
            $this->tambahBaru($barang, $jml_barang, $kd_transaksi, $tgl_transaksi, $tbl_transaksi, '');
        } else {
            $data_transaksi = unserialize($check[0]['daftar_barang']);
            if ($id == 'null') {
                $check_array = arraySearch($data_transaksi, 'kd_barang', $kd_barang);
                $this->updateBarangRequest($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 0);
            } else {
                $check_array = arraySearch($data_transaksi, 'kd_barang', $kd_barang);
                if ($check_array === false) {
                    $this->updateBarangRequest($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 1);
                } else {
                    $status_barang = $data_transaksi[$check_array]['status'];
                    if ($status_barang == 1) {
                        $this->updateBarangRequest($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 1);
                    } else {
                        r_error();
                    }
                }
            }
        }
    }

    public function tambahBaru($barang, $jml_barang, $kd_transaksi, $tgl_transaksi, $tbl_transaksi, $kd_customer)
    {
        $data_barang[] = array(
            'kd_barang' => $barang[0]['kd_barang'],
            'nama' => $barang[0]['nama'],
            'kd_suplier' => $barang[0]['kd_suplier'],
            'jml_barang' => $jml_barang,
            'satuan' => $barang[0]['satuan'],
            'status' => 0,
        );
        $data['kd_request'] = $kd_transaksi;
        $data['daftar_barang'] = serialize($data_barang);
        $data['tgl_request'] = $tgl_transaksi;

        $data['status'] = 1;
        $data['user_id'] = whoIAM()['id'];
        $data['status_terima'] = 0;
        $save = $this->main_model->store($tbl_transaksi, $data);
        if ($save) {
            r_success();
        }
    }

    public function updateBarangRequest($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, $status)
    {
        $data_barang = array(
            'kd_barang' => $barang[0]['kd_barang'],
            'nama' => $barang[0]['nama'],
            'kd_suplier' => $barang[0]['kd_suplier'],
            'jml_barang' => $jml_barang,
            'satuan' => $barang[0]['satuan'],
            'status' => $status,
        );

        if ($check_array === false) {
            array_push($data_transaksi, $data_barang);
        } else {
            $data_transaksi[$check_array] = $data_barang;
        }

        $data['daftar_barang'] = serialize($data_transaksi);
        $update = $this->main_model->update('request', $data, 'kd_request', $kd_transaksi);
        if ($update) {
            r_success();
        }
    }

    public function hapusBarang($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tbl_transaksi = 'request';
        $klm_transaksi = 'kd_request';
        $kd_transaksi = $obj->kd_request;
        $kd_barang = $obj->kd_barang;

        $data_transaksi = unserialize($this->main_model->gdo4p($tbl_transaksi, 'daftar_barang', $klm_transaksi, $kd_transaksi));
        if (count($data_transaksi) > 1) {

            $index = arraySearch($data_transaksi, 'kd_barang', $kd_barang);

            if ($id == 'null') {
                unset($data_transaksi[$index]);
            } else {
                if ($data_transaksi[$index]['status'] == 0) {
                    $update_barang = $this->main_model->updateSingleColumn('barang', 'jml_barang', "jml_barang-{$data_transaksi[$index]['jml_barang']}", 'kd_barang', $kd_barang);
                    if ($update_barang) {
                        unset($data_transaksi[$index]);
                    }
                } else {
                    unset($data_transaksi[$index]);
                }
            }

            $data['daftar_barang'] = serialize($data_transaksi);
            $update = $this->main_model->update($tbl_transaksi, $data, $klm_transaksi, $kd_transaksi);
            if ($update) {
                r_success();
            }
        }
    }

    public function simpanTransaksi($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tbl_transaksi = 'request';
        $klm_transaksi = 'kd_request';
        $kd_transaksi = $obj->kd_request;

        $data_transaksi = unserialize($this->main_model->gdo4p($tbl_transaksi, 'daftar_barang', $klm_transaksi, $kd_transaksi));
        $data_barang = array();

        $barang = $this->main_model->whereIn('barang', 'kd_barang', takeArrayValue($data_transaksi, 'kd_barang'));

        $index = 0;
        foreach ($data_transaksi as $key => $value) {
            if ($id == 'null') {
                $stock = $barang[arraySearch($barang, 'kd_barang', $value['kd_barang'])]['jml_barang'];
                $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['stock'] = ($stock - $value['jml_barang']);
                $data_barang[] = array(
                    'kd_barang' => $value['kd_barang'],
                    'jml_barang' => ($stock - $value['jml_barang']),
                );
            } else {
                if ($value['status'] == 1) {
                    $stock = $barang[arraySearch($barang, 'kd_barang', $value['kd_barang'])]['jml_barang'];
                    $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['stock'] = ($stock - $value['jml_barang']);
                    $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['status'] = 0;
                    $data_barang[] = array(
                        'kd_barang' => $value['kd_barang'],
                        'jml_barang' => ($stock - $value['jml_barang']),
                    );
                }
            }
            $index++;
        }

        if ($data_barang) {
            $update_item = $this->main_model->updateMultiple('barang', $data_barang, 'kd_barang');
            if ($update_item) {
                $data['daftar_barang'] = serialize($data_transaksi);
                $data['status'] = 0;
                $update = $this->main_model->update($tbl_transaksi, $data, $klm_transaksi, $kd_transaksi);
                if ($update) {
                    if ($id == 'null') {
                        logs("<b>Menambahkan Request</b> : Menambah request dengan nomor transaksi {$kd_transaksi}");
                    } else {
                        logs("<b>Memperbarui Request</b> : Memperbarui request dengan nomor transaksi {$kd_transaksi}");
                    }
                    r_success();
                }
            }
        } else {
            r_success();
        }
    }

    public function daftar_request()
    {
        if (role(['user', 'admin'], false)) {
            $this->load->view('admin/content/request/daftar_request');
        } else {
            $this->load->view('403');
        }
    }

    public function request_table()
    {
        $id = 'id_request';
        $table = 'request';
        $column = array(
            'id_request',
            'kd_request',
            'tgl_request',
            'user_id',
            'user_accepted',
            'daftar_barang',
            'status_terima',
        );
        show_table($id, $table, $column, 'null');
    }

    public function accept()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $request = $this->main_model->gda3p('request', 'id_request', $id);
        if ($request[0]['status_terima'] == 0) {
            $data['status_terima'] = 1;
            $data['user_accepted'] = whoIAM()['id'];
            $update = $this->main_model->update('request', $data, 'id_request', $id);
            if ($update) {
                logs("<b>Menerima Request</b> : Menerima request dengan nomor transaksi {$request[0]['kd_request']}");
                r_success();
            }
        } else {
            r_error();
        }
    }

    public function refuse()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $request = $this->main_model->gda3p('request', 'id_request', $id);
        $data_transaksi = unserialize($request[0]['daftar_barang']);

        $data_barang = array();
        $barang = $this->main_model->whereIn('barang', 'kd_barang', takeArrayValue($data_transaksi, 'kd_barang'));
        foreach ($data_transaksi as $key => $value) {
            $stock = $barang[arraySearch($barang, 'kd_barang', $value['kd_barang'])]['jml_barang'];
            $data_barang[] = array(
                'kd_barang' => $value['kd_barang'],
                'jml_barang' => ($stock + $value['jml_barang']),
            );
        }

        if ($data_barang) {
            $update_item = $this->main_model->updateMultiple('barang', $data_barang, 'kd_barang');
            if ($update_item) {
                $data['status_terima'] = 2;
                $update = $this->main_model->update('request', $data, 'id_request', $id);
                if ($update) {
                    logs("<b>Menolak Request</b> : Menolak request dengan nomor transaksi {$request[0]['kd_request']} barang telah dikembalikan");
                    r_success();
                }
            }
        } else {
            r_success();
        }

    }

}
