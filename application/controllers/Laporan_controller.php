<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function barang_masuk($month, $year)
    {
        if (role(['admin'], false)) {
            $data['m'] = to_month($month);
            $data['month'] = indoMonth();
            $data['year'] = $year;
            $data['indoMonth'] = indoMonth();
            $data['daftar_barang'] = $this->main_model->gda5p('barang_masuk', 'month(tgl_masuk)', $month, 'year(tgl_masuk)', $year);
            $this->load->view('admin/content/laporan/barang_masuk/index', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function barang_keluar($month, $year)
    {
        if (role(['admin'], false)) {
            $data['m'] = to_month($month);
            $data['month'] = indoMonth();
            $data['year'] = $year;
            $data['indoMonth'] = indoMonth();
            $data['daftar_barang'] = $this->main_model->gda5p('barang_keluar', 'month(tgl_keluar)', $month, 'year(tgl_keluar)', $year);
            $this->load->view('admin/content/laporan/barang_keluar/index', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function masukGraph($year)
    {

        $data = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $incoming = $this->main_model->gda3p('barang_masuk', 'year(tgl_masuk)', $year);
        foreach ($incoming as $key => $value) {
            $date = explode('-', $value['tgl_masuk']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data[$i] += array_sum(array_column(unserialize($value['daftar_barang']), 'jml_barang'));
                }
            }
        }

        foreach ($data as $key => $value) {
            $row['data'][] = $value;
        }

        $row['name'] = 'Barang Masuk';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }


    public function keluarGraph($year)
    {

        $data = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $incoming = $this->main_model->gda3p('barang_keluar', 'year(tgl_keluar)', $year);
        foreach ($incoming as $key => $value) {
            $date = explode('-', $value['tgl_keluar']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data[$i] += array_sum(array_column(unserialize($value['daftar_barang']), 'jml_barang'));
                }
            }
        }

        foreach ($data as $key => $value) {
            $row['data'][] = $value;
        }

        $row['name'] = 'Barang Keluar';
        $result = array();
        array_push($result, $row);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

    public function print($table, $kode){
        if($table == 'barang_masuk'){
            $data_transaksi = $this->main_model->gda3p($table, 'kd_brg_masuk', $kode);
            $data['data_transaksi'] = $data_transaksi;
            $data['date'] = explode('-', $data_transaksi[0]['tgl_masuk']);
            $data['daftar_barang'] = unserialize($data_transaksi[0]['daftar_barang']);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/barang_masuk/print', $data);
        }else if($table == 'barang_keluar'){
            $data_transaksi = $this->main_model->gda3p($table, 'kd_brg_keluar', $kode);
            $data['data_transaksi'] = $data_transaksi;
            $data['date'] = explode('-', $data_transaksi[0]['tgl_keluar']);
            $data['daftar_barang'] = unserialize($data_transaksi[0]['daftar_barang']);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/barang_keluar/print', $data);
        }else if($table == 'request'){
            $data_transaksi = $this->main_model->gda3p($table, 'kd_request', $kode);
            $data['data_transaksi'] = $data_transaksi;
            $data['date'] = explode('-', $data_transaksi[0]['tgl_request']);
            $data['daftar_barang'] = unserialize($data_transaksi[0]['daftar_barang']);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/request/print', $data);
        }else if($table == 'barang'){
            $data['barang'] = $this->main_model->gda1p($table);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/barang/print', $data);
        }
    }

    public function print_all($table, $month, $year){
        if($table == 'barang_masuk'){
            $data['month'] = $month;
            $data['year'] = $year;
            $data['data_transaksi'] = $this->main_model->gda7p($table, 'month(tgl_masuk)', to_m($month), 'year(tgl_masuk)', $year, 'status', 0);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/barang_masuk/print_all', $data);
        }else if($table == 'barang_keluar'){
            $data['month'] = $month;
            $data['year'] = $year;
            $data['data_transaksi'] = $this->main_model->gda7p($table, 'month(tgl_keluar)', to_m($month), 'year(tgl_keluar)', $year, 'status', 0);
            $this->load->library('fpdf');
            $this->load->view('admin/content/laporan/barang_keluar/print_all', $data);
        }
    }

    public function request($month, $year)
    {
        if (role(['admin'], false)) {
            $data['m'] = to_month($month);
            $data['month'] = indoMonth();
            $data['year'] = $year;
            $data['indoMonth'] = indoMonth();
            $data['daftar_barang'] = $this->main_model->gda5p('request', 'month(tgl_request)', $month, 'year(tgl_request)', $year);
            $this->load->view('admin/content/laporan/request/index', $data);
        } else {
            $this->load->view('403');
        }
    }

    public function requestGraph($year)
    {

        $data = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $data1 = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $data2 = array(
            1 => 0,
            2 => 0,
            3 => 0,
            4 => 0,
            5 => 0,
            6 => 0,
            7 => 0,
            8 => 0,
            9 => 0,
            10 => 0,
            11 => 0,
            12 => 0
        );

        $all = $this->main_model->gda3p('request', 'year(tgl_request)', $year);
        foreach ($all as $key => $v1) {
            $date = explode('-', $v1['tgl_request']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data[$i] += array_sum(array_column(unserialize($v1['daftar_barang']), 'jml_barang'));
                }
            }
        }

        $accept = $this->main_model->gda5p('request', 'year(tgl_request)', $year, 'status_terima', 1);
        foreach ($accept as $key => $v2) {
            $date = explode('-', $v2['tgl_request']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data1[$i] += array_sum(array_column(unserialize($v2['daftar_barang']), 'jml_barang'));
                }
            }
        }

        $refuse = $this->main_model->gda5p('request', 'year(tgl_request)', $year, 'status_terima', 2);
        foreach ($refuse as $key => $v3) {
            $date = explode('-', $v3['tgl_request']);
            for($i = 1; $i <= 12; $i++){
                if($date[1] == $i){
                    $data2[$i] += array_sum(array_column(unserialize($v3['daftar_barang']), 'jml_barang'));
                }
            }
        }

        foreach ($data as $key => $value) {
            $row['data'][] = $value;
        }
        foreach ($data1 as $key => $value) {
            $row1['data'][] = $value;
        }
        foreach ($data2 as $key => $value) {
            $row2['data'][] = $value;
        }

        $row['name'] = 'Total Permintaan';
        $row1['name'] = 'Diterima';
        $row2['name'] = 'Ditolak';
        $result = array();
        array_push($result, $row);
        array_push($result, $row1);
        array_push($result, $row2);
        print json_encode($result, JSON_NUMERIC_CHECK);
    }

}
