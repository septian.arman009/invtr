<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        $this->perPage = 10;
        auth();
    }

    public function error_handling()
    {
        $this->load->view('404');
    }

    public function index()
    {
        if (!isset($_COOKIE['theme_inv'])) {
            $data['theme'] = 'theme-teal';
        } else {
            $data['theme'] = $_COOKIE['theme_inv'];
        }

        if ($data['theme'] == 'theme-red') {
            $data['table_color'] = '#F44336';
        } else if ($data['theme'] == 'theme-pink') {
            $data['table_color'] = '#E91E63 ';
        } else if ($data['theme'] == 'theme-purple') {
            $data['table_color'] = '#9C27B0 ';
        } else if ($data['theme'] == 'theme-deep-purple') {
            $data['table_color'] = '#673AB7 ';
        } else if ($data['theme'] == 'theme-indigo') {
            $data['table_color'] = '#3F51B5 ';
        } else if ($data['theme'] == 'theme-blue') {
            $data['table_color'] = '#2196F3 ';
        } else if ($data['theme'] == 'theme-light-blue') {
            $data['table_color'] = '#03A9F4 ';
        } else if ($data['theme'] == 'theme-cyan') {
            $data['table_color'] = '#00BCD4 ';
        } else if ($data['theme'] == 'theme-teal') {
            $data['table_color'] = '#009688';
        } else if ($data['theme'] == 'theme-green') {
            $data['table_color'] = '#4CAF50 ';
        } else if ($data['theme'] == 'theme-light-green') {
            $data['table_color'] = '#8BC34A ';
        } else if ($data['theme'] == 'theme-lime') {
            $data['table_color'] = '#CDDC39 ';
        } else if ($data['theme'] == 'theme-yellow') {
            $data['table_color'] = '#ffe821 ';
        } else if ($data['theme'] == 'theme-amber') {
            $data['table_color'] = '#FFC107 ';
        } else if ($data['theme'] == 'theme-orange') {
            $data['table_color'] = '#FF9800 ';
        } else if ($data['theme'] == 'theme-deep-orange') {
            $data['table_color'] = '#FF5722 ';
        } else if ($data['theme'] == 'theme-brown') {
            $data['table_color'] = '#795548 ';
        } else if ($data['theme'] == 'theme-grey') {
            $data['table_color'] = '#9E9E9E ';
        } else if ($data['theme'] == 'theme-blue-grey') {
            $data['table_color'] = '#607D8B ';
        } else if ($data['theme'] == 'theme-black') {
            $data['table_color'] = '#000000 ';
        }
        $data['setting'] = $this->main_model->gda1p('pengaturan');
        $data['date_now'] = explode('-', date('Y-m-d'));
        $this->load->view('admin/content/main', $data);
    }

    public function home()
    {
        $this->load->view('admin/content/home/home');
    }

    public function log($page)
    {
        $total_row = $this->main_model->count('aktifitas', 'id_aktifitas');
        $data['all'] = ceil($total_row / 10);
        if ($page != 1) {
            $end = $this->perPage * $page;
            $start = $end - 10;
            $data['page'] = $page;
            if ($start > 0) {
                $data['logs'] = $this->main_model->limit($start, $end, 'aktifitas', 'id_aktifitas');
                $this->load->view('admin/content/home/log', $data);
            } else {
                $data['logs'] = '';
                $this->load->view('admin/content/home/log', $data);
            }
        } else {
            $data['page'] = $page;
            $data['logs'] = $this->main_model->limit(0, $this->perPage, 'aktifitas', 'id_aktifitas');
            $this->load->view('admin/content/home/log', $data);
        }
    }

    public function destroy($table, $column)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        if ($table == 'users' && $id == $_SESSION['invtr_in']['id']) {

        } else if (role(['admin'], true)) {
            if($table == 'users'){
                $this->main_model->destroy('aktifitas', $column, $id);
                $message = "<b>Menghapus Pengguna</b> : Menghapus pengguna dengan Nama : {$this->main_model->gdo4p($table, 'name', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
                
            }else if($table == 'customer'){
                $kd_customer = $this->main_model->gdo4p('customer', 'kd_customer', 'id_customer', $id);
                $check = $this->main_model->gda3pl('barang_keluar', 'daftar_barang', $kd_customer, 'id_brg_keluar', 'asc');
                if(!$check){
                    $message = "<b>Menghapus Barang</b> : Menghapus customer dengan Nama : {$this->main_model->gdo4p($table, 'nama', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'suplier'){
                $exist = $this->main_model->exixt5p('barang', 'kd_suplier', 'suplier', 'id_suplier', $id);
                if(!$exist){
                    $message = "<b>Menghapus Suplier</b> : Menghapus suplier dengan Nama : {$this->main_model->gdo4p($table, 'nama', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'barang'){
                $kd_barang = $this->main_model->gdo4p('barang', 'kd_barang', 'id_barang', $id);
                $check = $this->main_model->gda3pl('barang_masuk', 'daftar_barang', $kd_barang, 'id_brg_masuk', 'asc');
                $check1 = $this->main_model->gda3pl('barang_keluar', 'daftar_barang', $kd_barang, 'id_brg_keluar', 'asc');

                if(!$check && !$check1){
                    $message = "<b>Menghapus Barang</b> : Menghapus barang dengan Nama : {$this->main_model->gdo4p($table, 'nama', $column, $id)}";
                    $this->do_destroy($table, $column, $id, $message);
                }else{
                    r_error();
                }
            }else if($table == 'barang_masuk'){
                $message = "<b>Menghapus Barang Masuk</b> : Menghapus barang masuk dengan Kode : {$this->main_model->gdo4p($table, 'kd_brg_masuk', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }else if($table == 'barang_keluar'){
                $message = "<b>Menghapus Barang Keluar</b> : Menghapus barang keluar dengan Kode : {$this->main_model->gdo4p($table, 'kd_brg_keluar', $column, $id)}";
                $this->do_destroy($table, $column, $id, $message);
            }
        }
    }

    function do_destroy($table, $column, $id, $message){
        $destroy = $this->main_model->destroy($table, $column, $id);
        if($destroy){
            logs($message);
            r_success();
        }else{
            r_error();
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('invtr_in');
    }

    public function setTheme($theme)
    {
        $cookie = array(
            "name" => 'theme_inv',
            "value" => "$theme",
            "expire" => 7200,
            "secure" => false,
        );
        $this->input->set_cookie($cookie);
    }

    public function update_sys_mail()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $data['send_mail'] = $obj->email;
        $data['send_pass'] = $obj->password;
        if (role(['admin'], true)) {
            $update = $this->main_model->update('pengaturan', $data, 'id_pengaturan', 1);
            if ($update) {
                r_success();
            }
        }
    }

    public function exist_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;

        $check = $this->main_model->gda3p($table, $column, $value);
        if ($check) {
            r_success();
        }
    }

    public function single_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            if (!$check) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, $column, $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                if (!$check) {
                    r_success();
                }
            }
        }
    }

    public function triple_data_check()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $value = $obj->value;
        $column = $obj->column;
        $table = $obj->table;
        $table1 = $obj->table1;
        $table2 = $obj->table2;
        if ($id == 'null') {
            $check = $this->main_model->gda3p($table, $column, $value);
            $check1 = $this->main_model->gda3p($table1, $column, $value);
            $check2 = $this->main_model->gda3p($table2, $column, $value);
            if (!$check && !$check1 && !$check2) {
                r_success();
            }
        } else {
            $old = $this->main_model->gdo4p($table, $column, 'id', $id);
            if ($value == $old) {
                r_success();
            } else {
                $check = $this->main_model->gda3p($table, $column, $value);
                $check1 = $this->main_model->gda3p($table1, $column, $value);
                $check2 = $this->main_model->gda3p($table2, $column, $value);
                if (!$check && !$check1 && !$check2) {
                    r_success();
                }
            }
        }
    }

    public function get_data()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $column = $obj->column;
        $table = $obj->table;
        $data = $this->main_model->gda3p($table, $column, $id);
        if ($data) {
            r_success_data($data);
        }
    }

    public function search_ajax($column, $table)
    {
        $query = $this->input->get('query');
        $this->db->like($column, $query);
        $data = $this->db->get($table)->result();
        echo json_encode($data);
    }

}
