<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function form($form, $id, $param)
    {
        $data['id'] = $id;
        if ($form == 'user') {
            $data['role'] = $_SESSION['invtr_in']['role'];
            $data['roles'] = $this->main_model->gda1p('roles');
            $this->load->view('admin/content/master/user/form', $data);
        } else if ($form == 'customer') {
            $data['token'] = randomString(5);
            $data['kd_customer'] = generate_code('GVR', 'customer');
            $this->load->view('admin/content/master/customer/form', $data);
        } else if ($form == 'suplier') {
            $data['kd_suplier'] = generate_code('SP', 'suplier');
            $this->load->view('admin/content/master/suplier/form', $data);
        }else if ($form == 'barang') {
            $data['suplier'] = $this->main_model->gda1p('suplier');
            $data['kd_barang'] = generate_code('RC', 'barang');
            $this->load->view('admin/content/master/barang/form', $data);
        }
    }

    public function user()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/master/user/index');
        } else {
            $this->load->view('403');
        }
    }

    public function user_table()
    {
        $id = 'user_id';
        $table = 'users';
        $column = array(
            'user_id',
            'kd_user',
            'name',
            'email',
        );
        show_table($id, $table, $column, 'null');
    }

    public function user_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['name'] = ucwords($obj->name);
        $data['email'] = $obj->email;
        $data['role_id'] = $obj->role_id;
        
        if ($id == 'null') {
            $data['kd_user'] = generate_kdUser($obj->role_id);
            $data['password'] = md5($obj->password);
            $log_message = "Mendaftarkan pengguna baru dengan email : {$data['email']}";
            do_action($id, $table, 'user_id', $data, $log_message);
        } else {
            $log_message = "Update pengguna dengan email : {$data['email']}";
            do_action($id, $table, 'user_id', $data, $log_message);
        }
    }

    public function customer()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/master/customer/index');
        } else {
            $this->load->view('403');
        }
    }

    public function customer_table()
    {
        $id = 'id_customer';
        $table = 'customer';
        $column = array(
            'id_customer',
            'kd_customer',
            'nama',
            'alamat',
            'tlp',
            'pic'
        );
        show_table($id, $table, $column, 'null');
    }

    public function customer_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['kd_customer'] = ucwords($obj->kd_customer);
        $data['nama'] = ucwords($obj->nama);
        $data['alamat'] =ucwords($obj->alamat);
        $data['tlp'] = $obj->tlp;
        $data['pic'] = ucwords($obj->pic);
        $data['token'] = ucwords($obj->token);

        if ($id == 'null') {
            $log_message = "Mendaftarkan customer baru dengan kode : {$data['kd_customer']}";
            do_action($id, $table, 'id_customer', $data, $log_message);
        } else {
            $log_message = "Update customer dengan kode : {$data['kd_customer']}";
            do_action($id, $table, 'id_customer', $data, $log_message);
        }
    }

    public function suplier()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/master/suplier/index');
        } else {
            $this->load->view('403');
        }
    }

    public function suplier_table()
    {
        $id = 'id_suplier';
        $table = 'suplier';
        $column = array(
            'id_suplier',
            'kd_suplier',
            'nama',
            'alamat',
            'tlp'
        );
        show_table($id, $table, $column, 'null');
    }

    public function suplier_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['kd_suplier'] = ucwords($obj->kd_suplier);
        $data['nama'] = ucwords($obj->nama);
        $data['alamat'] =ucwords($obj->alamat);
        $data['tlp'] = $obj->tlp;

        if ($id == 'null') {
            $log_message = "Mendaftarkan suplier baru dengan kode : {$data['kd_suplier']}";
            do_action($id, $table, 'id_suplier', $data, $log_message);
        } else {
            $log_message = "Update suplier dengan kode : {$data['kd_suplier']}";
            do_action($id, $table, 'id_suplier', $data, $log_message);
        }
    }

    public function barang()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/master/barang/index');
        } else {
            $this->load->view('403');
        }
    }

    public function barang_table()
    {
        $id = 'id_barang';
        $table = 'barang';
        $column = array(
            'id_barang',
            'kd_barang',
            'kd_suplier',
            'nama',
            'satuan',
            'harga',
            'jml_barang'
        );
        show_table($id, $table, $column, 'null');
    }

    public function barang_action()
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $id = $obj->id;
        $table = $obj->table;
        $data['kd_barang'] = ucwords($obj->kd_barang);
        $data['kd_suplier'] = $obj->kd_suplier;
        $data['nama'] = ucwords($obj->nama);
        $data['satuan'] =$obj->satuan;
        $data['harga'] = to_int($obj->harga);
        $data['jml_barang'] = $obj->jml_barang;

        if ($id == 'null') {
            $log_message = "Mendaftarkan barang baru dengan kode : {$data['kd_barang']}";
            do_action($id, $table, 'id_barang', $data, $log_message);
        } else {
            $log_message = "Update barang dengan kode : {$data['kd_barang']}";
            do_action($id, $table, 'id_barang', $data, $log_message);
        }
    }
    
}
