<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi_controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('main_model');
        auth();
    }

    public function barang_masuk($id)
    {
        if (role(['admin'], false)) {
            $data['id'] = $id;
            if ($id == 'null') {
                $status = $this->main_model->limitWhere(0, 1, 'barang_masuk', 'status', 1, 'id_brg_masuk');
                if ($status) {
                    $data['kd_brg_masuk'] = $status[0]['kd_brg_masuk'];
                    $data['date'] = to_engdate($status[0]['tgl_masuk']);
                    $data['status'] = 1;
                    $data['daftar_barang'] = unserialize($status[0]['daftar_barang']);
                } else {
                    $data['kd_brg_masuk'] = generate_code_tnc('barang_masuk');
                    $data['date'] = to_engdate(date('Y-m-d'));
                    $data['status'] = 0;
                    $data['daftar_barang'] = array();
                }
            } else {
                $brg_masuk = $this->main_model->gda3p('barang_masuk', 'id_brg_masuk', $id);
                $data['kd_brg_masuk'] = $brg_masuk[0]['kd_brg_masuk'];
                $data['date'] = to_engdate($brg_masuk[0]['tgl_masuk']);
                $data['status'] = 1;
                $data['daftar_barang'] = unserialize($brg_masuk[0]['daftar_barang']);
            }

            $this->load->view('admin/content/transaksi/barang_masuk/index', $data);

        } else {
            $this->load->view('403');
        }
    }

    public function barang_keluar($id)
    {
        if(role(['admin'],false)){
            $data['id'] = $id;
            if($id == 'null'){
                $status = $this->main_model->limitWhere(0, 1,'barang_keluar', 'status', 1, 'id_brg_keluar');
                if($status){
                    $data['kd_brg_keluar'] = $status[0]['kd_brg_keluar'];
                    $data['date'] = to_engdate($status[0]['tgl_keluar']);
                    $data['status'] = 1;
                    $data['daftar_barang'] = unserialize($status[0]['daftar_barang']);
                }else{
                    $data['kd_brg_keluar'] = generate_code_tnc('barang_keluar');
                    $data['date'] = to_engdate(date('Y-m-d'));
                    $data['status'] = 0;
                    $data['daftar_barang'] = array();
                }
            }else{
                $brg_keluar = $this->main_model->gda3p('barang_keluar', 'id_brg_keluar', $id);
                $data['kd_brg_keluar'] = $brg_keluar[0]['kd_brg_keluar'];
                $data['date'] = to_engdate($brg_keluar[0]['tgl_keluar']);
                $data['status'] = 1;    
                $data['daftar_barang'] = unserialize($brg_keluar[0]['daftar_barang']);
            }
            $this->load->view('admin/content/transaksi/barang_keluar/index', $data);
        }else{
            $this->load->view('403');
        }
    }

    public function cari_barang()
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select nama from barang where nama like '%$name%'")->result_array();
        $dataName = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $dataName[] = $value['nama'];
            }
        }
        echo json_encode($dataName);
    }

    public function cari_customer()
    {
        $name = $this->input->get('query');
        $data = $this->db->query("select nama from customer where nama like '%$name%'")->result_array();
        $dataName = array();
        if ($data) {
            foreach ($data as $key => $value) {
                $dataName[] = $value['nama'];
            }
        }
        echo json_encode($dataName);
    }

    public function tambahBarang($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $kd_transaksi = $obj->kd_transaksi;
        $tbl_transaksi = $obj->tbl_transaksi;
        $klm_transaksi = $obj->klm_transaksi;
        $kd_barang = $obj->kd_barang;
        $jml_barang = $obj->jml_barang;
        $total_harga = to_int($obj->total_harga);
        $tgl_transaksi = to_date_mysql($obj->tgl_transaksi);

        $check = $this->main_model->gda3p($tbl_transaksi, $klm_transaksi, $kd_transaksi);
        $barang = $this->main_model->gda3p('barang', 'kd_barang', $kd_barang);
        if (!$check) {
            if ($tbl_transaksi == 'barang_masuk') {
                $this->tambahBaru($barang, $jml_barang, $kd_transaksi, $tgl_transaksi, $tbl_transaksi, $total_harga, '');
            } else {
                $kd_customer = $obj->kd_customer;
                $this->tambahBaru($barang, $jml_barang, $kd_transaksi, $tgl_transaksi, $tbl_transaksi, $total_harga, $kd_customer);
            }
        } else {
            $data_transaksi = unserialize($check[0]['daftar_barang']);
            if ($id == 'null') {
                if ($tbl_transaksi == 'barang_masuk') {
                    $check_array = arraySearch($data_transaksi, 'kd_barang', $kd_barang);
                    $this->updateBarangMasuk($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, $total_harga, 0);
                } else {
                    $kd_customer = $obj->kd_customer;
                    $check_array = arraySearch2($data_transaksi, 'kd_customer', $kd_customer, 'kd_barang', $kd_barang);
                    $this->updateBarangKeluar($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 0, $kd_customer, $total_harga);
                }
            } else {
                if ($tbl_transaksi == 'barang_masuk') {
                    $check_array = arraySearch($data_transaksi, 'kd_barang', $kd_barang);
                    if ($check_array === false) {
                        $this->updateBarangMasuk($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, $total_harga, 1);
                    } else {
                        $status_barang = $data_transaksi[$check_array]['status'];
                        if ($status_barang == 1) {
                            $this->updateBarangMasuk($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi,$total_harga, 1);
                        } else {
                            r_error();
                        }
                    }
                } else {
                    $kd_customer = $obj->kd_customer;
                    $check_array = arraySearch2($data_transaksi, 'kd_customer', $kd_customer, 'kd_barang', $kd_barang);
                    if ($check_array === false) {
                        $this->updateBarangKeluar($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 1, $kd_customer, $total_harga);
                    } else {
                        $status_barang = $data_transaksi[$check_array]['status'];
                        if ($status_barang == 1) {
                            $this->updateBarangKeluar($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, 1, $kd_customer, $total_harga);
                        } else {
                            r_error();
                        }
                    }
                }
            }
        }
    }

    public function tambahBaru($barang, $jml_barang, $kd_transaksi, $tgl_transaksi, $tbl_transaksi, $total_harga, $kd_customer)
    {
        if ($tbl_transaksi == 'barang_masuk') {
            $data_barang[] = array(
                'kd_barang' => $barang[0]['kd_barang'],
                'nama' => $barang[0]['nama'],
                'kd_suplier' => $barang[0]['kd_suplier'],
                'jml_barang' => $jml_barang,
                'satuan' => $barang[0]['satuan'],
                'total_harga' => $total_harga,
                'status' => 0,
            );
            $data['kd_brg_masuk'] = $kd_transaksi;
            $data['daftar_barang'] = serialize($data_barang);
            $data['tgl_masuk'] = $tgl_transaksi;
        } else {
            $data_barang[] = array(
                'kd_customer' => $kd_customer,
                'kd_barang' => $barang[0]['kd_barang'],
                'nama' => $barang[0]['nama'],
                'jml_barang' => $jml_barang,
                'satuan' => $barang[0]['satuan'],
                'total_harga' => $total_harga,
                'status' => 0,
            );
            $data['kd_brg_keluar'] = $kd_transaksi;
            $data['daftar_barang'] = serialize($data_barang);
            $data['tgl_keluar'] = $tgl_transaksi;
        }

        $data['status'] = 1;
        $data['user_id'] = whoIAM()['id'];

        $save = $this->main_model->store($tbl_transaksi, $data);
        if ($save) {
            r_success();
        }
    }

    public function updateBarangMasuk($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, $total_harga, $status)
    {
        $data_barang = array(
            'kd_barang' => $barang[0]['kd_barang'],
            'nama' => $barang[0]['nama'],
            'kd_suplier' => $barang[0]['kd_suplier'],
            'jml_barang' => $jml_barang,
            'satuan' => $barang[0]['satuan'],
            'total_harga' => $total_harga,
            'status' => $status,
        );

        if ($check_array === false) {
            array_push($data_transaksi, $data_barang);
        } else {
            $data_transaksi[$check_array] = $data_barang;
        }

        $data['daftar_barang'] = serialize($data_transaksi);
        $data['user_updated'] = whoIAM()['id'];
        $update = $this->main_model->update('barang_masuk', $data, 'kd_brg_masuk', $kd_transaksi);
        if ($update) {
            r_success();
        }
    }

    public function updateBarangKeluar($barang, $jml_barang, $check_array, $data_transaksi, $kd_transaksi, $status, $kd_customer, $total_harga)
    {
        $barang = array(
            'kd_customer' => $kd_customer,
            'kd_barang' => $barang[0]['kd_barang'],
            'nama' => $barang[0]['nama'],
            'jml_barang' => $jml_barang,
            'satuan' => $barang[0]['satuan'],
            'total_harga' => $total_harga,
            'status' => $status,
        );

        if ($check_array === false) {
            array_push($data_transaksi, $barang);
        } else {
            $data_transaksi[$check_array] = $barang;
        }

        $data['daftar_barang'] = serialize($data_transaksi);
        $data['user_updated'] = whoIAM()['id'];
        $update = $this->main_model->update('barang_keluar', $data, 'kd_brg_keluar ', $kd_transaksi);
        if ($update) {
            r_success();
        }
    }

    public function hapusBarang($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tbl_transaksi = $obj->tbl_transaksi;
        $klm_transaksi = $obj->klm_transaksi;
        $kd_transaksi = $obj->kd_transaksi;
        $kd_barang = $obj->kd_barang;

        $data_transaksi = unserialize($this->main_model->gdo4p($tbl_transaksi, 'daftar_barang', $klm_transaksi, $kd_transaksi));
        if (count($data_transaksi) > 1) {

            if ($tbl_transaksi == 'barang_masuk') {
                $index = arraySearch($data_transaksi, 'kd_barang', $kd_barang);
            } else {
                $kd_customer = $obj->kd_customer;
                $index = arraySearch2($data_transaksi, 'kd_customer', $kd_customer, 'kd_barang', $kd_barang);
            }

            if ($id == 'null') {
                unset($data_transaksi[$index]);
            } else {
                if ($data_transaksi[$index]['status'] == 0) {
                    if ($tbl_transaksi == 'barang_masuk') {
                        $update_barang = $this->main_model->updateSingleColumn('barang', 'jml_barang', "jml_barang-{$data_transaksi[$index]['jml_barang']}", 'kd_barang', $kd_barang);
                    } else {
                        $update_barang = $this->main_model->updateSingleColumn('barang', 'jml_barang', "jml_barang+{$data_transaksi[$index]['jml_barang']}", 'kd_barang', $kd_barang);
                    }
                    if ($update_barang) {
                        unset($data_transaksi[$index]);
                    }
                } else {
                    unset($data_transaksi[$index]);
                }
            }

            $data['daftar_barang'] = serialize($data_transaksi);
            $data['user_updated'] = whoIAM()['id'];
            $update = $this->main_model->update($tbl_transaksi, $data, $klm_transaksi, $kd_transaksi);
            if ($update) {
                r_success();
            }
        }
    }

    public function simpanTransaksi($id)
    {
        $json = file_get_contents("php://input");
        $obj = json_decode($json);
        $tbl_transaksi = $obj->tbl_transaksi;
        $klm_transaksi = $obj->klm_transaksi;
        $kd_transaksi = $obj->kd_transaksi;

        $data_transaksi = unserialize($this->main_model->gdo4p($tbl_transaksi, 'daftar_barang', $klm_transaksi, $kd_transaksi));
        $data_barang = array();

        if ($tbl_transaksi == 'barang_masuk') {
            $barang = $this->main_model->whereIn('barang', 'kd_barang', takeArrayValue($data_transaksi, 'kd_barang'));
        } else {
            $kd_customer = takeArrayValue($data_transaksi, 'kd_customer');
            if ($id == 'null') {
                $arrayIcode = takeArrayValue($data_transaksi, 'kd_barang');
                $qtyPerIcode = takeArrayValue1($data_transaksi, 'kd_barang', 'jml_barang');
                foreach ($arrayIcode as $key => $value) {
                    $stock[$value] = $this->main_model->gdo4p('barang', 'jml_barang', 'kd_barang', $value);
                    $data_barang[] = array(
                        'kd_barang' => $value,
                        'jml_barang' => $stock[$value] - $qtyPerIcode[$value],
                    );
                }
            } else {
                $statusEQ1 = takeArrayValueWhere($data_transaksi, 'kd_barang', 'jml_barang', 'status', 1);
                $arrayIcode = takeArrayValue($statusEQ1, 'kd_barang');
                $qtyPerIcode = takeArrayValue1($statusEQ1, 'kd_barang', 'jml_barang');
                foreach ($arrayIcode as $key => $value) {
                    $stock[$value] = $this->main_model->gdo4p('barang', 'jml_barang', 'kd_barang', $value);
                    $data_barang[] = array(
                        'kd_barang' => $value,
                        'jml_barang' => $stock[$value] - $qtyPerIcode[$value],
                    );
                }
            }
        }

        $index = 0;
        foreach ($data_transaksi as $key => $value) {
            if ($id == 'null') {
                if ($tbl_transaksi == 'barang_masuk') {
                    $stock = $barang[arraySearch($barang, 'kd_barang', $value['kd_barang'])]['jml_barang'];
                    $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['stock'] = ($stock + $value['jml_barang']);
                    $data_barang[] = array(
                        'kd_barang' => $value['kd_barang'],
                        'jml_barang' => ($stock + $value['jml_barang']),
                    );
                } else {
                    $tnc_data[arraySearch2($data_transaksi, 'kd_customer', $kd_customer[$index], 'kd_barang', $value['kd_barang'])]['stock'] = $stock[$value['kd_barang']];
                    $stock[$value['kd_barang']] = ($stock[$value['kd_barang']] - $value['jml_barang']);
                    if ($stock[$value['kd_barang']] < 0) {
                        r_error();
                        die();
                    }
                }
            } else {
                if ($value['status'] == 1) {
                    if ($tbl_transaksi == 'barang_masuk') {
                        $stock = $barang[arraySearch($barang, 'kd_barang', $value['kd_barang'])]['jml_barang'];
                        $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['stock'] = ($stock + $value['jml_barang']);
                        $data_transaksi[arraySearch($data_transaksi, 'kd_barang', $value['kd_barang'])]['status'] = 0;
                        $data_barang[] = array(
                            'kd_barang' => $value['kd_barang'],
                            'jml_barang' => ($stock + $value['jml_barang']),
                        );
                    } else {
                        $data_transaksi[arraySearch2($data_transaksi, 'kd_customer', $kd_customer[$index], 'kd_barang', $value['kd_barang'])]['stock'] = $stock[$value['kd_barang']];
                        $stock[$value['kd_barang']] = ($stock[$value['kd_barang']] - $value['jml_barang']);
                        $data_transaksi[arraySearch2($data_transaksi, 'kd_customer', $kd_customer[$index], 'kd_barang', $value['kd_barang'])]['status'] = 0;
                        if ($stock[$value['kd_barang']] < 0) {
                            r_error();
                            die();
                        }
                    }
                }
            }
            $index++;
        }

        if ($data_barang) {
            $update_item = $this->main_model->updateMultiple('barang', $data_barang, 'kd_barang');
            if ($update_item) {
                $data['daftar_barang'] = serialize($data_transaksi);
                $data['status'] = 0;
                $data['user_updated'] = whoIAM()['id'];
                $update = $this->main_model->update($tbl_transaksi, $data, $klm_transaksi, $kd_transaksi);
                if ($update) {
                    if ($tbl_transaksi == 'barang_masuk') {
                        if ($id == 'null') {
                            logs("<b>Menambahkan Barang Masuk</b> : Menambah barang masuk dengan nomor transaksi {$kd_transaksi}");
                        } else {
                            logs("<b>Memperbarui Barang Masuk</b> : Memperbarui barang masuk dengan nomor transaksi {$kd_transaksi}");
                        }
                    } else {
                        if ($id == 'null') {
                            logs("<b>Menambahkan Barang Keluar</b> : Menambah barang keluar dengan nomor transaksi {$kd_transaksi}");
                        } else {
                            logs("<b>Memperbarui Barang Keluar</b> : Memperbarui barang keluar dengan nomor transaksi {$kd_transaksi}");
                        }
                    }
                    r_success();
                }
            }
        } else {
            r_success();
        }
    }

    public function daftar_brg_masuk()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/transaksi/barang_masuk/daftar_brg_masuk');
        } else {
            $this->load->view('403');
        }
    }

    public function brgMasuk_table()
    {
        $id = 'id_brg_masuk';
        $table = 'barang_masuk';
        $column = array(
            'id_brg_masuk',
            'kd_brg_masuk',
            'tgl_masuk',
            'user_id',
            'user_updated',
            'daftar_barang'
        );
        show_table($id, $table, $column, 'null');
    }

    public function daftar_brg_keluar()
    {
        if (role(['admin'], false)) {
            $this->load->view('admin/content/transaksi/barang_keluar/daftar_brg_keluar');
        } else {
            $this->load->view('403');
        }
    }

    public function brgKeluar_table()
    {
        $id = 'id_brg_keluar';
        $table = 'barang_keluar';
        $column = array(
            'id_brg_keluar',
            'kd_brg_keluar',
            'tgl_keluar',
            'user_id',
            'user_updated',
            'daftar_barang'
        );
        show_table($id, $table, $column, 'null');
    }
}
