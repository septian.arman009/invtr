function signin() {
	var data = {
		email: $("#email").val(),
		password: $("#password").val()
	};

	postData("auth_controller/signin", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				window.location = "admin";
			} else {
				swal("Oops..!", "Username or password doesn't match.", "error");
			}
		}
	});
}

function f_check() {
	var email = $("#email").val();
	if (email != '') {
		if (ValidateEmail(email, "#send_mail", "#invalid_email")) {
			email_check(email);
		}
	} else {
		$("#invalid_email").hide();
		$("#invalid").hide();
	}
}

function email_check(email) {
	var data = {
		value: email,
		column: 'email',
		table: 'users'
	}

	postData('auth_controller/exist_check', data, function (err, response) {
		if (response) {
			var status = response.status;
			if (status == 'success') {
				$("#invalid").hide();
				$('#send_mail').removeAttr('disabled');
			} else {
				$("#invalid").show();
				$("#send_mail").attr('disabled', 'disabled');
			}
		} else {
			console.log('ini error : ', err);
		}
	});
}

function send_token() {
	$("#send_mail").hide();
	$("#loading").show();
	var data = {
		email: $("#email").val()
	};

	postData("auth_controller/send_token", data, function (err, response) {
		if (response) {
			if (response.status == "success") {
				$("#send_mail").show();
				$("#send_mail").attr('disabled', 'disabled');
				$("#email").val('');
				$("#loading").hide();
				swal("Success", "Link sent.", "success");
			} else {
				$("#send_mail").show();
				$("#loading").hide();
				swal("Oops..!", "Link sent failed.", "error");
			}
		}
	});
}

function check_password() {
	var password = $("#password").val();
	var confirm = $("#confirm").val();

	var passlen = password.length;
	var conlen = confirm.length;

	if (passlen > 0 && passlen < 8) {
		$("#passless").show();
	} else {
		$("#passless").hide();
	}

	if (conlen > 0 && conlen < 8) {
		$("#conless").show();
		$("#unmatch").hide();
	} else {
		$("#conless").hide();
		$("#unmatch").hide();
		if (password.length >= 8 && confirm.length >= 8) {
			if (password == confirm) {
				$("#unmatch").hide();
				$("#save").removeAttr('disabled');
			} else {
				$("#unmatch").show();
				$("#save").attr('disabled', 'disabled');
			}
		} else {
			$("#save").attr('disabled', 'disabled');
		}
	}
}