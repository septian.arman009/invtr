-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 03:27 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_invtr`
--

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id_aktifitas` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `aktifitas` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id_aktifitas`, `user_id`, `name`, `aktifitas`, `created_at`) VALUES
(1, 1, 'Admin', '<b>Menghapus Pengguna</b> : Menghapus pengguna dengan Nama : Arman Septian', '2019-07-17 20:12:13'),
(2, 1, 'Admin', 'Mendaftarkan pengguna baru dengan email : user@gmail.com', '2019-07-17 20:12:55'),
(3, 12, 'Arman', '<b>Menambahkan Request</b> : Menambah request dengan nomor transaksi REQ1707190005', '2019-07-17 20:19:37'),
(4, 1, 'Admin', '<b>Menerima Request</b> : Menerima request dengan nomor transaksi ', '2019-07-17 20:23:29'),
(5, 1, 'Admin', '<b>Menolak Request</b> : Menolak request dengan nomor transaksi REQ1707190005 barang telah dikembalikan', '2019-07-17 20:25:33'),
(6, 12, 'Arman', '<b>Menambahkan Request</b> : Menambah request dengan nomor transaksi REQ1707190006', '2019-07-17 20:27:16'),
(7, 1, 'Admin', '<b>Menerima Request</b> : Menerima request dengan nomor transaksi REQ1707190006', '2019-07-17 20:27:25');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(10) NOT NULL,
  `kd_barang` varchar(10) NOT NULL,
  `kd_suplier` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `harga` int(10) NOT NULL,
  `jml_barang` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `kd_barang`, `kd_suplier`, `nama`, `satuan`, `harga`, `jml_barang`) VALUES
(1, 'RC01', 'SP01', 'MP-25', 'pcs', 500000, 8890),
(2, 'RC02', 'SP02', 'PP-25', 'pcs', 550000, 9000);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_brg_keluar` int(10) NOT NULL,
  `kd_brg_keluar` varchar(20) NOT NULL,
  `tgl_keluar` date NOT NULL,
  `daftar_barang` text NOT NULL,
  `status` int(1) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_updated` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_brg_masuk` int(10) NOT NULL,
  `kd_brg_masuk` varchar(20) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `daftar_barang` text NOT NULL,
  `status` int(1) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_updated` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(10) NOT NULL,
  `kd_customer` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(15) NOT NULL,
  `pic` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `kd_customer`, `nama`, `alamat`, `tlp`, `pic`) VALUES
(1, 'GVR01', 'Mayora', 'Bekasi Jaya', '089517227009', 'Arman'),
(2, 'GVR02', 'Tunas Alvin', 'Bekasi Jaya', '0895172209', 'Arman');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id_pengaturan` int(10) NOT NULL,
  `protocol` varchar(30) NOT NULL,
  `smtp_host` varchar(30) NOT NULL,
  `smtp_port` int(10) NOT NULL,
  `send_mail` varchar(50) NOT NULL,
  `send_pass` varchar(30) NOT NULL,
  `email` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id_pengaturan`, `protocol`, `smtp_host`, `smtp_port`, `send_mail`, `send_pass`, `email`) VALUES
(1, 'smtp', 'ssl://smtp.gmail.com', 465, 'atasan.dcontrol@gmail.com', 'januari1993', 'on');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id_request` int(10) NOT NULL,
  `kd_request` varchar(20) NOT NULL,
  `tgl_request` date NOT NULL,
  `daftar_barang` text NOT NULL,
  `status` int(1) NOT NULL,
  `status_terima` int(1) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_accepted` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id_request`, `kd_request`, `tgl_request`, `daftar_barang`, `status`, `status_terima`, `user_id`, `user_accepted`) VALUES
(5, 'REQ1707190005', '2019-07-17', 'a:1:{i:0;a:7:{s:9:\"kd_barang\";s:4:\"RC01\";s:4:\"nama\";s:5:\"MP-25\";s:10:\"kd_suplier\";s:4:\"SP01\";s:10:\"jml_barang\";s:2:\"90\";s:6:\"satuan\";s:3:\"pcs\";s:6:\"status\";i:0;s:5:\"stock\";i:8810;}}', 0, 2, 12, 1),
(6, 'REQ1707190006', '2019-07-17', 'a:1:{i:0;a:7:{s:9:\"kd_barang\";s:4:\"RC01\";s:4:\"nama\";s:5:\"MP-25\";s:10:\"kd_suplier\";s:4:\"SP01\";s:10:\"jml_barang\";s:2:\"10\";s:6:\"satuan\";s:3:\"pcs\";s:6:\"status\";i:0;s:5:\"stock\";i:8890;}}', 0, 1, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `display_name`, `created_at`) VALUES
(1, 'admin', 'Admin', '2019-06-20 19:48:03'),
(2, 'user', 'User', '2019-06-20 19:48:03');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` int(10) NOT NULL,
  `kd_suplier` varchar(10) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `kd_suplier`, `nama`, `alamat`, `tlp`) VALUES
(1, 'SP01', 'Era Prima', 'Bekasi Jaya', '089517227009'),
(2, 'SP02', 'SIndomas', 'Bekasi Jaya', '089517227009');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id_token` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `kd_user` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role_id` int(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `kd_user`, `name`, `email`, `password`, `role_id`, `created_at`) VALUES
(1, 'ADM0001', 'Admin', 'admin@gmail.com', '5456db00ec97635f7d998e3290a01b6d', 1, '2018-05-25 00:00:00'),
(12, 'USR0012', 'Arman', 'user@gmail.com', '5456db00ec97635f7d998e3290a01b6d', 2, '2019-07-17 20:12:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id_aktifitas`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_brg_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_brg_masuk`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id_pengaturan`);

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id_request`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id_token`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id_aktifitas` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id_brg_keluar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id_brg_masuk` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id_request` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `suplier`
--
ALTER TABLE `suplier`
  MODIFY `id_suplier` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id_token` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD CONSTRAINT `aktifitas_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `tokens`
--
ALTER TABLE `tokens`
  ADD CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`email`) REFERENCES `users` (`email`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
